#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "FreeSans"
\font_sans "default" "FreeSerif"
\font_typewriter "default" "FreeMono"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
We want to estimate the radius of the star by its measured absolute magnitude
 
\begin_inset Formula $V^{t}$
\end_inset

 and temperature 
\begin_inset Formula $T^{t}$
\end_inset

.
 To do so we need a comparison star, with known absolute magnitude 
\begin_inset Formula $V^{c}$
\end_inset

, radius 
\begin_inset Formula $R^{c}$
\end_inset

 and temperature 
\begin_inset Formula $T^{c}$
\end_inset

.
 We begin again with Planck's function 
\begin_inset Formula $B_{\lambda}\left(T\right)$
\end_inset

.
 The flux emitted from a surface element of the star 
\begin_inset Formula $\ell$
\end_inset

 is proportional to integration of 
\begin_inset Formula $B_{\lambda}\left(T\right)$
\end_inset

 over the different wavelengths with the filter curve:
\begin_inset Formula 
\[
\ell_{V}\propto\int_{0}^{\infty}B_{\lambda}\left(T\right)\cdot f_{V}\left(\lambda\right)\cdot d\lambda
\]

\end_inset

The power of light emitted from the star, i.e.
 the luminosity 
\begin_inset Formula $L_{V}$
\end_inset

, is proportional to the flux 
\begin_inset Formula $\ell_{V}$
\end_inset

 times the surface area 
\begin_inset Formula $A$
\end_inset

.
 Therefore:
\begin_inset Formula 
\[
L_{V}\propto A\ell_{V}=4\pi R^{2}\ell_{V}
\]

\end_inset

where we took the area 
\begin_inset Formula $A$
\end_inset

 to be the area of a sphere with radius 
\begin_inset Formula $R$
\end_inset

.
 We can look at the ratio of the luminosity of the variable star and compare
 it to a known star:
\begin_inset Formula 
\[
\frac{L_{V}^{t}}{L_{V}^{c}}=\frac{4\pi R_{t}^{2}\ell_{V}^{t}}{4\pi R_{c}^{2}\ell_{V}^{c}}=\frac{R_{t}^{2}\int_{0}^{\infty}B_{\lambda}\left(T^{t}\right)\cdot f_{V}\left(\lambda\right)\cdot d\lambda}{R_{c}^{2}\int_{0}^{\infty}B_{\lambda}\left(T^{c}\right)\cdot f_{V}\left(\lambda\right)\cdot d\lambda}
\]

\end_inset

We can relate the ratio of the luminosity 
\begin_inset Formula $\frac{L_{V}^{t}}{L_{V}^{c}}$
\end_inset

 to the difference in the absolute magnitudes of the stars:
\begin_inset Formula 
\[
\frac{L_{V}^{t}}{L_{V}^{c}}=100^{\frac{V^{c}-V^{t}}{5}}
\]

\end_inset

These give the relation:
\begin_inset Formula 
\[
\frac{R_{t}^{2}\int_{0}^{\infty}B_{\lambda}\left(T^{t}\right)\cdot f_{V}\left(\lambda\right)\cdot d\lambda}{R_{c}^{2}\int_{0}^{\infty}B_{\lambda}\left(T^{c}\right)\cdot f_{V}\left(\lambda\right)\cdot d\lambda}=100^{\frac{V^{c}-V^{t}}{5}}
\]

\end_inset

and 
\begin_inset Formula $R_{t}$
\end_inset

 can be extracted:
\begin_inset Formula 
\[
R_{t}=R_{c}\sqrt{\frac{\int_{0}^{\infty}B_{\lambda}\left(T^{c}\right)\cdot f_{V}\left(\lambda\right)\cdot d\lambda}{\int_{0}^{\infty}B_{\lambda}\left(T^{t}\right)\cdot f_{V}\left(\lambda\right)\cdot d\lambda}}\sqrt{100^{\frac{V^{c}-V^{t}}{5}}}
\]

\end_inset

We assumed that 
\begin_inset Formula $T^{t,c}$
\end_inset

, 
\begin_inset Formula $R_{c}$
\end_inset

 and 
\begin_inset Formula $V^{c,t}$
\end_inset

 are known, and by numerical evaluation of the integral we can find the
 radius of the target star.
\end_layout

\end_body
\end_document
