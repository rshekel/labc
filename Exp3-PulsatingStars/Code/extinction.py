"""
Fix the reddening due to extinction

See the graph for extinction vs distance here:
https://stilism.obspm.fr/reddening?frame=icrs&vlong=270%3A43%3A07.65&ulong=deg&vlat=21%3A31%3A50.90&ulat=deg&valid=

In order to convert parallax aecsecond angle to pc, see the relation here:
http://spiff.rit.edu/classes/phys301/lectures/parallax/parallax.html
"""
import matplotlib.pyplot as plt
import numpy as np
from uncertainties import ufloat

REDDENING_PATH = 'reddening.csv'


def plot_extinction_vs_distance():
    data = np.loadtxt(REDDENING_PATH, skiprows=1, delimiter=';')
    d, E, d_err, E_min, E_max = np.split(data, 5)

    errs = np.zeros((2, len(d)))
    # errs[0]

    fig, ax = plt.subplot()

    # ax.errorbar


def estimate_E_B_V():
    p1 = (600, 0.077)
    p2 = (870, 0.087)

    m = (p2[1] - p1[1]) / (p2[0] - p1[0])
    a = p1[1] - m * p1[0]

    error = ufloat(0, 0.063)

    linear_func = lambda x: m * x + a + error

    distance_mas = ufloat(0.7541012207577649, 0.014584912)
    distance_as = distance_mas * 1e-3
    distance_pc = 1 / distance_as

    print(f'distance pc: {distance_pc}')

    E_B_V = linear_func(distance_pc)

    print(E_B_V)


def main():
    # plot_extinction_vs_distance()
    estimate_E_B_V()


if __name__ == '__main__':
    main()
