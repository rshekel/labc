import glob
import matplotlib.pyplot as plt
import numpy as np
from astropy.io import fits


SATURATED = 5.75e4
MAX_MAX_ALLOWED = 4.5e4
MIN_MAX_ALLOWED = 2e4

MASTER_BIAS_PATH = "../Masters/master_bias.fit"
MASTER_DARK_PATH = "../Masters/master_dark.fit"
MASTER_FLAT_B_PATH = "../Masters/master_flat_B.fit"
MASTER_FLAT_V_PATH = "../Masters/master_flat_V.fit"


def remove_bias(img):
    bias = fits.open(MASTER_BIAS_PATH)[0].data
    return img - bias


def remove_dark(img):
    dark = fits.open(MASTER_BIAS_PATH)[0].data
    return img - dark


def show_image(img, title='', vmin=None, vmax=None):
    fig, ax = plt.subplots()
    ax.imshow(img, vmin=vmin, vmax=vmax)
    ax.set_title(title)
    fig.show()


def gen_master_flat(color_filter='V'):
    paths = glob.glob(f"../Observation/flat/{color_filter}/*flat.fit")

    files = [fits.open(path)[0] for path in paths]

    # Filter good pictures
    files = list(filter(lambda x: x.is_image, files))
    files = list(filter(lambda x: x.data.max() < MAX_MAX_ALLOWED and x.data.max() > MIN_MAX_ALLOWED, files))

    imgs = [f.data for f in files]

    # remove DCs
    imgs = [remove_bias(img) for img in imgs]

    # We should not remove dark master from the flat images since the flat images are taken with very short
    # exposure time. It is possible to normalize the dark master to the actual exposure time, but it would only
    # add noise
    # imgs = [remove_dark(img) for img in imgs]

    # Normalize
    imgs = [img / (img.max()) for img in imgs]

    # np.dstack should probably do it also, but whatever...
    img_stack = np.zeros(shape=imgs[0].shape + (len(imgs),))
    for i, img in enumerate(imgs):
        img_stack[:, :, i] = img

    master_flat = np.median(img_stack, axis=2)

    out_file = files[0]
    out_file.data = master_flat
    if color_filter == 'B':
        out_file.writeto(MASTER_FLAT_B_PATH)
    elif color_filter == 'V':
        out_file.writeto(MASTER_FLAT_V_PATH)

    show_image(master_flat, title='master flat', vmin=0.83, vmax=1)


def gen_master_dark():
    paths = glob.glob("../Observation/dark/*dark.fit")
    files = [fits.open(path)[0] for path in paths]

    # Filter good pictures
    files = list(filter(lambda x: x.is_image, files))

    imgs = [f.data for f in files]
    imgs = [remove_bias(img) for img in imgs]

    # np.dstack should probably do it also, but whatever...
    img_stack = np.zeros(shape=imgs[0].shape + (len(imgs),))
    for i, img in enumerate(imgs):
        img_stack[:, :, i] = img

    master_dark = np.median(img_stack, axis=2)

    show_image(master_dark, title='master dark')

    out_file = files[0]
    out_file.data = master_dark
    out_file.writeto(MASTER_DARK_PATH)


def gen_master_bias():
    paths = glob.glob("../Observation/bias/*bias.fit")
    files = [fits.open(path)[0] for path in paths]

    # Filter good pictures
    files = list(filter(lambda x: x.is_image, files))
    imgs = [f.data for f in files]

    # np.dstack should probably do it also, but whatever...
    img_stack = np.zeros(shape=imgs[0].shape + (len(imgs),))
    for i, img in enumerate(imgs):
        img_stack[:, :, i] = img

    master_bias = np.median(img_stack, axis=2)

    show_image(master_bias, title='master bias')

    out_file = files[0]
    out_file.data = master_bias
    out_file.writeto(MASTER_BIAS_PATH)


def gen_masters():
    gen_master_bias()
    gen_master_dark()
    gen_master_flat()


def main():
    gen_masters()


if __name__ == "__main__":
    main()
    plt.show()
