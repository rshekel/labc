import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
import os
import glob

MASTER_BIAS_PATH = "../Masters/master_bias.fit"
MASTER_DARK_PATH = "../Masters/master_dark.fit"
MASTER_FLAT_B_PATH = "../Masters/master_flat_B.fit"
MASTER_FLAT_V_PATH = "../Masters/master_flat_V.fit"

master_bias = fits.open(MASTER_BIAS_PATH)[0]
master_dark = fits.open(MASTER_DARK_PATH)[0]
master_flat_b = fits.open(MASTER_FLAT_B_PATH)[0]
master_flat_v = fits.open(MASTER_FLAT_V_PATH)[0]


def fix_with_masters(f, out_path):
    f.data = f.data.astype('float64') - master_dark.data
    f.data -= master_bias.data
    if f.header['FILTER'] == 'B':
       f.data /= master_flat_b.data
    elif f.header['FILTER'] == 'V':
        f.data /= master_flat_v.data
    else:
        raise Exception("What filter is this?!")

    f.writeto(out_path)


def main():
    base_dir = r"../Observation/science"
    # dir_part = r"/B/1"
    # dir_part = r"/B/2"
    # dir_part = r"/V/1"
    dir_part = r"/V/2"

    final_dir = base_dir + dir_part + "/post_master_gauge"

    paths = glob.glob(base_dir + dir_part + "/*.fts")
    os.mkdir(final_dir)
    print(paths)

    for path in paths:
        print(path)
        f = fits.open(path)[0]
        final_path = os.path.join(final_dir, os.path.basename(path))
        fix_with_masters(f, final_path)


if __name__ == "__main__":
    main()
    plt.show()