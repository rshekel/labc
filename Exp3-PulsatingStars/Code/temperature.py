"""
The filter curves are taken from here:
https://old.aip.de/en/research/facilities/stella/instruments/data/johnson-ubvri-filter-curves
"""
import typing

import matplotlib.pyplot as plt
import numpy as np
import uncertainties.unumpy as unp
from PyAstronomy import pyasl
from uncertainties import ufloat

from parse_data import calc_rel_photometry, get_const_star_power, get_magnitudes

# Physical consts, in SI

h = 6.626e-34
c = 299792458
K_B = 1.380649e-23

FILTER_CURVE_B = 'Bessel_B-1.txt'
FILTER_CURVE_V = 'Bessel_V-1.txt'

B_filter_curve: typing.Optional[np.ndarray] = None
V_filter_curve: typing.Optional[np.ndarray] = None
all_wavelength_filter_curve: typing.Optional[np.ndarray] = None


def flux_ratio_to_magnitude_diff(ratio):
    return -2.5 * unp.log10(ratio)


def magnitude_diff_to_flux_ratio(diff):
    return 100 ** (-diff / 5)


def load_filter_curves():
    global B_filter_curve, V_filter_curve, all_wavelength_filter_curve
    B_filter_curve = np.loadtxt(FILTER_CURVE_B)
    V_filter_curve = np.loadtxt(FILTER_CURVE_V)

    # Change nm->m
    B_filter_curve[:, 0] /= 1e9
    V_filter_curve[:, 0] /= 1e9

    # Change percentage->ratio
    B_filter_curve[:, 1] /= 100
    V_filter_curve[:, 1] /= 100

    # This is probably wrong
    # B_filter_curve[:, 1] /= np.sum(B_filter_curve[:, 1])
    # V_filter_curve[:, 1] /= np.sum(V_filter_curve[:, 1])

    # Create the all wavelength filter curve
    all_wavelength: np.ndarray = np.arange(100, 5000, 0.5) * 1e-9
    all_wavelength = all_wavelength[::-1]
    all_wavelength_filter_curve = np.array([all_wavelength, np.ones(len(all_wavelength))])
    all_wavelength_filter_curve = all_wavelength_filter_curve.transpose()


def plot_filter_curves():
    fig, ax = plt.subplots()
    ax.plot(B_filter_curve[:, 0], B_filter_curve[:, 1], 'b', label='B filter')
    ax.plot(V_filter_curve[:, 0], V_filter_curve[:, 1], 'g', label='V filter')

    ax.set_xlabel('wavelength [nm]')
    ax.legend()

    plt.show()


def calc_ratio_per_temp_vec():
    Ts = np.arange(1e3, 50e3, 1e2)
    ratio = np.zeros(len(Ts))
    for i, T in enumerate(Ts):
        B = calc_expected_flux(T, B_filter_curve)
        V = calc_expected_flux(T, V_filter_curve)
        ratio[i] = B / V

    should_fix_constant = False
    if should_fix_constant:
        C_B_V = 0.3
        print('ratio', ratio)
        # B_V_diff = flux_ratio_to_magnitude_diff(ratio)
        B_V_diff = -2.5 * np.log10(ratio)
        B_V_diff += C_B_V
        ratio = magnitude_diff_to_flux_ratio(B_V_diff)

    return Ts, ratio


def Planck_B(wavelength, T):
    a = 2 * h * c ** 2 / (wavelength ** 5)
    b = np.exp(h * c / (wavelength * K_B * T)) - 1
    return a / b


def calc_expected_flux(T, filter_curve):
    ls = filter_curve[:, 0]
    d_l = -(ls[1] - ls[0])
    transfer = filter_curve[:, 1]

    integrand = Planck_B(ls, T) * transfer

    integral_value = np.sum(integrand * d_l)

    debug = False
    if debug:
        fig, ax = plt.subplots()
        ax.plot(ls, Planck_B(ls, T) * transfer, '.')
        print(integral_value)

    return integral_value


def get_B_V_ratio():
    times_B, powers_B, times_V, powers_V = calc_rel_photometry()

    ratio = powers_B / powers_V

    fig, ax = plt.subplots()
    ax.errorbar(times_B, unp.nominal_values(ratio), unp.std_devs(ratio), marker='.', linestyle='None')

    # We found the counts which are proportional to the number of photons, but we need to find the ratio in the energies

    # These values are estimated from the filter curves plots
    # center_wavelength_B = ufloat(4.213e-7, 0.1e-7)
    # center_wavelength_V = ufloat(5.22e-7, 0.05e-7)

    # These values are calculated using calc_mean_std_photon_energy
    # This is also similar to the data given in the curve filter site
    center_wavelength_B = ufloat(4.38e-07, 0.34e-07)
    center_wavelength_V = ufloat(5.5e-07, 0.4e-07)

    center_E_B = h * c / center_wavelength_B
    center_E_V = h * c / center_wavelength_V

    # print('energy per wavelength ratio', center_E_B / center_E_V)

    B_flux = powers_B * center_E_B
    V_flux = powers_V * center_E_V

    ratio = B_flux / V_flux

    # Note: It is not really constant. For now we take the mean value
    fig, ax = plt.subplots()
    ax.errorbar(times_B, unp.nominal_values(ratio), unp.std_devs(ratio), marker='.', linestyle='None')

    return times_B, ratio


def calc_temp_by_B_V_ratio(B_V_ratio, Ts, ratio_per_T, should_plot=False):
    sub = np.abs(ratio_per_T - B_V_ratio.nominal_value)
    best_index = np.argmin(sub)

    sub = np.abs(ratio_per_T - (B_V_ratio.nominal_value + B_V_ratio.std_dev))
    err_max_index = np.argmin(sub)

    sub = np.abs(ratio_per_T - (B_V_ratio.nominal_value - B_V_ratio.std_dev))
    err_min_index = np.argmin(sub)

    T = Ts[best_index]
    T_max = Ts[err_max_index]
    T_min = Ts[err_min_index]

    # print(f'T: {T}, max: {T_max}, min: {T_min}')

    if should_plot:
        fig, ax = plt.subplots()
        ax.plot(Ts, sub)
    return T, T_min, T_max


def analyze_temperature_bad():
    plt.rcParams['font.size'] = 14
    times, ratios = get_B_V_ratio()

    should_fix_extinction = False
    if should_fix_extinction:
        ratios = fix_extinction(ratios)

    Ts = np.zeros(len(ratios))
    Ts_min = np.zeros(len(ratios))
    Ts_max = np.zeros(len(ratios))

    Temp_vec, ratio_per_T = calc_ratio_per_temp_vec()

    for i, ratio in enumerate(ratios):
        T, T_min, T_max = calc_temp_by_B_V_ratio(ratio, Temp_vec, ratio_per_T)
        Ts[i] = T
        Ts_min[i] = T_min
        Ts_max[i] = T_max

    errs = np.zeros((2, len(Ts)))
    errs[0, :] = Ts - Ts_min
    errs[1, :] = Ts_max - Ts

    fig, ax = plt.subplots()
    ax.errorbar(times, Ts, errs, fmt='.')

    ax.set_title('Temperature vs time')
    ax.set_xlabel('time [hour]')
    ax.set_ylabel('Temperature [K]')

    uTs = unp.uarray(Ts, Ts_max - Ts)
    # Mean temperature:
    print(np.mean(uTs))

    B_V_measured = flux_ratio_to_magnitude_diff(ratios)
    print('B-V measured', B_V_measured)


def print_known_temperature():
    """
    Calc the 'known' temperature
    See https://en.wikipedia.org/wiki/Color_index for the equation
    The data is taken from here: https://www.aavso.org/vsx/index.php?view=detail.top&oid=108776
    """
    B_minus_V = 0.26
    T2 = 4600 * (1 / (0.92 * B_minus_V + 1.7) + 1 / (0.92 * B_minus_V + 0.62))
    print(T2)


def analyze_temperature_bad2():
    # Powers is counts ratio
    times_B, powers_B, times_V, powers_V = calc_rel_photometry()
    times = times_B

    m_B = 12.58
    m_V = 11.88

    RHS = powers_B / powers_V * 100 ** ((m_V - m_B) / 5)

    print(RHS)
    print('flux ratio: ', magnitude_diff_to_flux_ratio(0.37))
    print('flux ratio: ', magnitude_diff_to_flux_ratio(0.26))

    should_fix_RHS = True
    if should_fix_RHS:
        RHS = RHS / 0.71121351 * 0.7870457

    should_fix_extinction = True
    if should_fix_extinction:
        E_B_V = ufloat(0.1, 0.06)
        RHS *= 100 ** (-E_B_V / 5)

    Ts = np.zeros(len(RHS))
    Ts_min = np.zeros(len(RHS))
    Ts_max = np.zeros(len(RHS))

    Temp_vec, ratio_per_T = calc_ratio_per_temp_vec()

    for i, ratio in enumerate(RHS):
        T, T_min, T_max = calc_temp_by_B_V_ratio(ratio, Temp_vec, ratio_per_T)
        Ts[i] = T
        Ts_min[i] = T_min
        Ts_max[i] = T_max

    errs = np.zeros((2, len(Ts)))
    errs[0, :] = Ts - Ts_min
    errs[1, :] = Ts_max - Ts

    plot_temperature_vs_time(times, Ts, errs)

    # uTs = unp.uarray(Ts, Ts_max - Ts)

    std = np.std(unp.nominal_values(Ts))
    mean_T = ufloat(np.mean(unp.nominal_values(Ts)), std)
    # Mean temperature:
    print(mean_T)


def analyze_temperature():
    times_B, mag_B, times_V, mag_V = get_magnitudes()
    times = times_B

    B_V_diff = mag_B - mag_V

    should_fix_extinction = True
    if should_fix_extinction:
        E_B_V = ufloat(0.1, 0.06)
        B_V_diff += E_B_V

    Ts = np.zeros(len(B_V_diff))
    Ts_min = np.zeros(len(B_V_diff))
    Ts_max = np.zeros(len(B_V_diff))

    r = pyasl.Ramirez2005()

    for i, B_V in enumerate(B_V_diff):
        Ts[i] = r.colorToTeff('B-V', B_V.nominal_value, 0, ignoreRange=True)
        Ts_max[i] = r.colorToTeff('B-V', B_V.nominal_value - B_V.std_dev, 0, ignoreRange=True)
        Ts_min[i] = r.colorToTeff('B-V', B_V.nominal_value + B_V.std_dev, 0, ignoreRange=True)

    errs = np.zeros((2, len(Ts)))
    errs[0, :] = Ts - Ts_min
    errs[1, :] = Ts_max - Ts

    print(errs[0, :])
    print(errs[1, :])

    errs = np.abs(errs)

    # fig, ax = plt.subplots()
    # ax.plot(times, Ts)
    plot_temperature_vs_time(times, Ts, errs)

    # uTs = unp.uarray(Ts, Ts_max - Ts)

    std = np.std(unp.nominal_values(Ts))
    mean_T = ufloat(np.mean(unp.nominal_values(Ts)), std)
    # Mean temperature:
    print(mean_T)

    return times, Ts, errs


def plot_temperature_vs_time(times, Ts, errs):
    plt.rcParams['font.size'] = 14

    fig, ax = plt.subplots()
    ax.errorbar(times, Ts, errs, fmt='.')

    # ax.set_title('Temperature vs time')
    ax.set_xlabel('time [hour]')
    ax.set_ylabel('temperature [K]')

    fig.savefig("../Figures/temperature.png")


def fix_extinction(B_V_ratio_O):
    # B_V_O is observed ratio from the data
    # B_V is the real ratio
    # See page 61 eq. 3.3 for the relation between difference in magnitudes and flux ratio

    # This is the extrapolated value
    E_B_V = ufloat(0.1, 0.06)

    # This is the value Balshay used, not sure how he got it
    # E_B_V = ufloat(0.057, 0.037)

    # This tests the 0 extinction case
    # E_B_V = ufloat(0, 0)

    # See here how to fix the redenning
    # http://astronomy.swin.edu.au/cosmos/I/Interstellar+Reddening

    B_V_O = -2.5 * unp.log10(B_V_ratio_O)
    B_V = B_V_O + E_B_V
    B_V_ratio = 100 ** (-B_V / 5)
    return B_V_ratio


def calc_mean_std_photon_energy(filter_curve):
    ls: np.ndarray = filter_curve[:, 0]
    d_l = -(ls[1] - ls[0])
    transfer = filter_curve[:, 1]
    transfer = transfer.copy()
    transfer /= np.sum(transfer * d_l)

    mean = np.sum(ls * transfer * d_l)
    var = np.sum((ls ** 2) * transfer * d_l) - mean ** 2

    return ufloat(mean, np.sqrt(var))


def print_photon_wavelength():
    l_B = calc_mean_std_photon_energy(B_filter_curve)
    l_V = calc_mean_std_photon_energy(V_filter_curve)

    print(f'l_B: {l_B}')
    print(f'l_V: {l_V}')


def test_calc_expected_flux():
    """
    There is an example in the book p. 78 but I'm not sure what it means

    See values here
    https://en.wikipedia.org/wiki/Color_index
    """
    # T = 3840
    # T = 5777
    # T = 5940
    # T = 7000
    # T = 30000

    data = [[42000, 30000, 9790, 7300, 5940, 5150, 3840], [-0.33, -0.3, -0.02, 0.3, 0.58, 0.81, 1.4]]

    fig, ax = plt.subplots()
    ax.plot(data[0], data[1], 'x')

    theoretical_func = lambda B_V: 4600 * (1 / (0.92 * B_V + 1.7) + 1 / (0.92 * B_V + 0.62))
    ys = np.linspace(data[1][0] - 0.1, data[1][-1], 200)
    xs = theoretical_func(ys)
    ax.plot(xs, ys, label='theoretical')

    # This value is obtained by the graph
    C_B_V = 0.3

    calculated_B_V = np.zeros(len(data[0]))

    for i, T in enumerate(data[0]):
        B_V_ratio = calc_expected_flux(T, B_filter_curve) / calc_expected_flux(T, V_filter_curve)
        B_V_diff = -2.5 * unp.log10(B_V_ratio)
        calculated_B_V[i] = B_V_diff
        # B_V_diff += C_B_V
        ax.plot(T, B_V_diff, 'r*')
        print(B_V_diff)

    data = np.array(data)
    Ts = data[0, :]
    expected_B_V = data[1, :]
    diff = expected_B_V - calculated_B_V
    fig, ax = plt.subplots()
    ax.plot(Ts, diff, '.')


def plot_Planck_B():
    """
    This should plot the figure appears in p. 69
    It works fine
    """
    fig, ax = plt.subplots()

    for T in [4000, 5777, 7000]:
        ls = np.arange(100, 1400, 0.5) * 1e-9
        flux = Planck_B(ls, T)

        ax.plot(ls, flux, label=f'T={T} K')
    ax.legend()


def calc_C_B_V_const_by_calibration_star():
    times_B, upowers_B, times_V, upowers_V = get_const_star_power()

    # This is constant
    B_V_flux_ratio = upowers_B / upowers_V

    debug = True
    if debug:
        fig, ax = plt.subplots()
        ax.errorbar(times_B, unp.nominal_values(B_V_flux_ratio), unp.std_devs(B_V_flux_ratio), fmt='.')

    B_V_flux_ratio = B_V_flux_ratio.mean()
    # But how do we know the B-C of the calibrated star?


def plot_B_V_vs_time():
    times_B, mag_B, times_V, mag_V = get_magnitudes()

    B_V_diff = mag_B - mag_V

    fig, ax = plt.subplots()
    ax.errorbar(times_B, unp.nominal_values(B_V_diff), yerr=unp.std_devs(B_V_diff), marker='.', linestyle='None',
                label='B-V')

    mean = B_V_diff.mean().nominal_value
    std = np.std(unp.nominal_values(B_V_diff))

    mean_B_V = ufloat(mean, std)

    print(f'mean B-V {mean_B_V}')


def main():
    load_filter_curves()

    # plot_filter_curves()
    # plt.show()

    # plot_Planck_B()
    # print_photon_wavelength()
    # test_calc_expected_flux()
    # print_known_temperature()
    # calc_C_B_V_const_by_calibration_star()
    # plot_B_V_vs_time()
    analyze_temperature()
    plt.show()


if __name__ == '__main__':
    main()
