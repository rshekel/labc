import astropy.time as atime
import matplotlib.pyplot as plt
import numpy as np
import uncertainties
import uncertainties.unumpy as unp
from astropy.timeseries import LombScargle
from scipy.interpolate import interp1d

B1_PATH = "../Data/Set3/B1.xls"
B2_PATH = "../Data/Set3/B2.xls"
V1_PATH = "../Data/Set3/V1.xls"
V2_PATH = "../Data/Set3/V2.xls"

VAR_STAR_FIG_PATH = "../Figures/VarStar_meas.png"
CONST_STAR_FIG_PATH = "../Figures/ConstStar_meas.png"
FFT_FIG_PATH = "../Figures/FFT.png"
LOMB_FIG_PATH = "../Figures/lomb.png"

NAMES = open(B1_PATH).readline().split('\t')


plt.rcParams['font.size'] = 14


def get_times_and_powers(path1, path2, power_col, time_col=5):
    times1 = np.loadtxt(path1, skiprows=1, delimiter="\t", usecols=(time_col,))
    powers1 = np.loadtxt(path1, skiprows=1, delimiter="\t", usecols=(power_col,))
    source_radius = np.loadtxt(path1, skiprows=1, delimiter="\t", usecols=(7,))[0]  # Should be 5
    sky_per_pixel = np.loadtxt(path1, skiprows=1, delimiter="\t", usecols=(power_col + 5,))
    sky_signal1 = np.pi * (source_radius ** 2) * sky_per_pixel

    times2 = np.loadtxt(path2, skiprows=1, delimiter="\t", usecols=(time_col,))
    powers2 = np.loadtxt(path2, skiprows=1, delimiter="\t", usecols=(power_col,))
    source_radius = np.loadtxt(path2, skiprows=1, delimiter="\t", usecols=(7,))[0]  # Should be 5
    sky_per_pixel = np.loadtxt(path2, skiprows=1, delimiter="\t", usecols=(power_col + 5,))
    sky_signal2 = np.pi * (source_radius ** 2) * sky_per_pixel

    powers = np.append(powers1, powers2)
    times = np.append(times1, times2)
    sky_signal = np.append(sky_signal1, sky_signal2)

    times = [atime.Time(t, format='jd') for t in times]
    t0 = times[0]
    times = [(t - t0).sec / 3600 for t in times]

    errs = np.sqrt(sky_signal + powers)
    powers = unp.uarray(powers, errs)
    return np.array(times), powers


def calc_rel_photometry():
    times_B, upowers_B = get_times_and_powers(B1_PATH, B2_PATH, 26, 5)
    times_V, upowers_V = get_times_and_powers(V1_PATH, V2_PATH, 26, 5)

    _, upowers_B1 = get_times_and_powers(B1_PATH, B2_PATH, 43, 5)  # First const star - B filter
    _, upowers_B2 = get_times_and_powers(B1_PATH, B2_PATH, 60, 5)  # Second const star - B filter
    usum_const_powers_B = upowers_B2  # + upowers_B2
    _, upowers_V1 = get_times_and_powers(V1_PATH, V2_PATH, 43, 5)  # First const star - V filter
    _, upowers_V2 = get_times_and_powers(V1_PATH, V2_PATH, 60, 5)  # Second const star - V filter
    usum_const_powers_V = upowers_V2  # + upowers_V2

    upowers_B /= usum_const_powers_B
    upowers_V /= usum_const_powers_V

    return times_B, upowers_B, times_V, upowers_V


def get_const_star_power():
    times_B, upowers_B1 = get_times_and_powers(B1_PATH, B2_PATH, 43, 5)  # First const star - B filter
    times_V, upowers_V1 = get_times_and_powers(V1_PATH, V2_PATH, 43, 5)  # First const star - V filter

    return times_B, upowers_B1, times_V, upowers_V1


def show_var_star(rel_photometry=True):
    plt.rcParams["figure.figsize"] = (8, 5)
    plt.rcParams["axes.labelsize"] = 18

    fig, ax = plt.subplots()

    if rel_photometry:
        # ax.set_title("Var star rel photometry")
        times_B, powers_B, times_V, powers_V = calc_rel_photometry()  # powers are count_t / count_c
        rel_mag_B = -2.5*unp.log10(powers_B)  # m_t - m_c (B)
        rel_mag_V = -2.5*unp.log10(powers_V)  # m_t - m_c (V)
        # Source for magnitudes: http://simbad.u-strasbg.fr/simbad/sim-coo?Coord=18+02+52.51+%2B21+31+50.9&CooFrame=FK5&CooEpoch=2000&CooEqui=2000&CooDefinedFrames=none&Radius=2&Radius.unit=arcmin&submit=submit+query&CoordList=
        # We expect mag_B to be 13.60 and mag_V to be 13.35
        mag_B = rel_mag_B + 12.58  # m_c from site
        mag_V = rel_mag_V + 11.88  # m_c from site

        ax.errorbar(times_B, unp.nominal_values(mag_B), yerr=unp.std_devs(mag_B), marker='.', linestyle='None',
                    color='b', label='Variable star - B filter')
        ax.errorbar(times_V, unp.nominal_values(mag_V), yerr=unp.std_devs(mag_V), marker='.', linestyle='None',
                    color='g', label='Variable star - V filter')
        ax.set_ylabel('magnitude')

    else:
        # ax.set_title("Var star abs photometry")
        times_B, powers_B = get_times_and_powers(B1_PATH, B2_PATH, 26, 5)
        times_V, powers_V = get_times_and_powers(V1_PATH, V2_PATH, 26, 5)

        ax.errorbar(times_B, unp.nominal_values(powers_B), yerr=unp.std_devs(powers_B), marker='.', linestyle='None',
                    color='b', label='Var star power - B filter')
        ax.errorbar(times_V, unp.nominal_values(powers_V), yerr=unp.std_devs(powers_V), marker='.', linestyle='None',
                    color='g', label='Var star power - V filter')
        ax.set_ylabel('counts')

    ax.legend()
    ax.set_xlabel('time (hour)')
    fig.savefig(VAR_STAR_FIG_PATH)
    fig.show()


def show_const_stars():
    plt.rcParams["figure.figsize"] = (8, 5)
    plt.rcParams["axes.labelsize"] = 18

    times_B1, powers_B1 = get_times_and_powers(B1_PATH, B2_PATH, 43, 5)  # First const star - B filter
    times_B2, powers_B2 = get_times_and_powers(B1_PATH, B2_PATH, 60, 5)  # Second const star - B filter
    times_V1, powers_V1 = get_times_and_powers(V1_PATH, V2_PATH, 43, 5)  # First const star - V filter
    times_V2, powers_V2 = get_times_and_powers(V1_PATH, V2_PATH, 60, 5)  # Second const star - V filter

    powers_B2 /= np.max(unp.nominal_values(powers_B2))
    powers_V2 /= np.max(unp.nominal_values(powers_V2))

    fig, ax = plt.subplots()
    ax.errorbar(times_B2, unp.nominal_values(powers_B2), yerr=unp.std_devs(powers_B2), marker='.', linestyle='None',
                color='b', label='Normalized const star - B filter')
    # ax.errorbar(times_B2, unp.nominal_values(powers_B2), yerr=unp.std_devs(powers_B2), marker='.', linestyle='None',
    #             color='g', label='Second const star - B filter')
    ax.errorbar(times_V2, unp.nominal_values(powers_V2), yerr=unp.std_devs(powers_V2), marker='.', linestyle='None',
                color='g', label='Normalized const star - V filter')
    # ax.errorbar(times_V2, unp.nominal_values(powers_V2), yerr=unp.std_devs(powers_V2), marker='.', linestyle='None',
    #             color='c', label='Second const star - V filter')
    ax.legend()
    ax.set_xlabel('time (hour)')
    ax.set_ylabel('normalized counts')
    fig.savefig(CONST_STAR_FIG_PATH)
    fig.show()
    fig.show()


def calc_period():
    plt.rcParams["figure.figsize"] = (8, 5)
    plt.rcParams["font.size"] = 14
    times_B, powers_B, times_V, powers_V = calc_rel_photometry()
    freq_B, fpower_B = LombScargle(times_B, unp.nominal_values(powers_B), unp.std_devs(powers_B)).autopower(
        minimum_frequency=0.1,
        maximum_frequency=3,
        samples_per_peak=15)
    freq_V, fpower_V = LombScargle(times_V, unp.nominal_values(powers_V), unp.std_devs(powers_V)).autopower(
        minimum_frequency=0.1,
        maximum_frequency=3,
        samples_per_peak=15)
    powers_B -= powers_B.mean()
    powers_V -= powers_V.mean()

    flag = False
    if flag:
        fig, ax = plt.subplots(2)
        ax[0].plot(freq_B, fpower_B, "*")
        ax[1].plot(freq_V, fpower_V, "*")
        fig.show()

    if True:
        fig, ax = plt.subplots()
        ax.plot(freq_B, fpower_B, "*b", label='B filter')
        ax.plot(freq_V, fpower_V, "*g", label='V filter')
        ax.set_xlabel("frequency (1/hour)")
        ax.set_ylabel("Lomb-Scargle amplitude")
        ax.legend()
        fig.savefig(LOMB_FIG_PATH)
        fig.show()

    return freq_B[np.argmax(fpower_B)]


def calc_period2(interpolate=True, try_dense=True):
    plt.rcParams["figure.figsize"] = (8, 5)
    plt.rcParams["axes.labelsize"] = 18
    times_B, powers_B, times_V, powers_V = calc_rel_photometry()
    powers_B = unp.nominal_values(powers_B)
    powers_V = unp.nominal_values(powers_V)
    powers_B -= powers_B.mean()
    powers_V -= powers_V.mean()

    if interpolate:
        # This doesn't really help to getting better resolution in fourier space, for that we need a larger total T,
        # and we can't interpolate that, so it is kind of hopeless... so how does the LombScargle do it???
        times_res = 1
        f = interp1d(times_B, powers_B)
        times_B = np.linspace(times_B[0], times_B[-1], times_B.shape[0] * times_res)
        powers_B = f(times_B)

        f2 = interp1d(times_V, powers_V)
        times_V = np.linspace(times_V[0], times_V[-1], times_V.shape[0] * times_res)
        powers_V = f2(times_V)

    fpower_B = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(powers_B)))
    freqs_B = np.fft.fftshift(np.fft.fftfreq(len(fpower_B), times_B[1] - times_B[0]))

    fpower_V = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(powers_V)))
    freqs_V = np.fft.fftshift(np.fft.fftfreq(len(fpower_V), times_V[1] - times_V[0]))

    if True:
        fig, ax = plt.subplots()
        ax.plot(freqs_B, np.abs(fpower_B), '*b', label='B filter')
        ax.plot(freqs_V, np.abs(fpower_V), '*g', label='V filter')
        ax.set_xlabel("frequency (1/hour)")
        ax.set_xlim((0, 5))
        ax.set_ylabel("FFT amplitude")
        ax.legend()
        fig.savefig(FFT_FIG_PATH)
        fig.show()

    flag = False
    if flag:
        fig, ax = plt.subplots(2)
        ax[0].plot(freqs_B, np.abs(fpower_B), '.', color='b')
        ax[1].plot(freqs_V, np.abs(fpower_V), '.', color='b')

        if try_dense:
            for erase in [30, 40, 50, 60]:
                powers_B = powers_B[:-erase]
                fpower_B = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(powers_B)))
                freqs_B = np.fft.fftshift(np.fft.fftfreq(len(fpower_B), times_B[1] - times_B[0]))

                powers_V = powers_V[:-erase]
                fpower_V = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(powers_V)))
                freqs_V = np.fft.fftshift(np.fft.fftfreq(len(fpower_V), times_V[1] - times_V[0]))
                ax[0].plot(freqs_B, np.abs(fpower_B), '.', color='b')
                ax[1].plot(freqs_V, np.abs(fpower_V), '.', color='b')

        fig.show()


def calc_abs_mag():
    time_col = 5
    times1 = np.loadtxt(B1_PATH, skiprows=1, delimiter="\t", usecols=(time_col,))
    times2 = np.loadtxt(B2_PATH, skiprows=1, delimiter="\t", usecols=(time_col,))
    times = np.append(times1, times2)
    times = [atime.Time(t, format='jd') for t in times]
    t0 = times[0]
    times = [(t - t0).sec / 3600 for t in times]
    T = times[-1] - times[0]
    std = (1/T) / 2  # Half a unit Shnata
    max_freq = calc_period()
    max_freq = uncertainties.ufloat(max_freq, std)
    print("max_freq is", max_freq, "iter / hour")
    print("max_freq is", max_freq/3600, "Hz")
    period = 1 / max_freq  # hours
    period_days = period / 24
    print("period is ", period * 60, "minutes")
    print("period is ", period_days, "days")

    # PL relation from https://arxiv.org/pdf/1904.08101.pdf
    abs_mag = uncertainties.ufloat(-2.94, 0.06) * unp.log10(period_days) - uncertainties.ufloat(1.34, 0.06)
    print("Absolute magnitude:", abs_mag)
    return abs_mag


def get_magnitudes():
    times_B, powers_B, times_V, powers_V = calc_rel_photometry()
    rel_mag_B = -2.5 * unp.log10(powers_B)  # m_t - m_c (V)
    rel_mag_V = -2.5 * unp.log10(powers_V)  # m_t - m_c (V)
    # Source for magnitudes: http://simbad.u-strasbg.fr/simbad/sim-coo?Coord=18+02+52.51+%2B21+31+50.9&CooFrame=FK5&CooEpoch=2000&CooEqui=2000&CooDefinedFrames=none&Radius=2&Radius.unit=arcmin&submit=submit+query&CoordList=
    # We expect mag_B to be approx. 13.60 and mag_V to be approx 13.35
    mag_B = rel_mag_B + 12.58  # m_c from site
    mag_V = rel_mag_V + 11.88  # m_c from site
    return times_B, mag_B, times_V, mag_V


def calc_distance():
    abs_mag = calc_abs_mag()
    _, _, _, mag_V = get_magnitudes()
    rel_mag:uncertainties.ufloat = np.mean(mag_V)

    # rel_mag.set_std_dev(np.std(unp.nominal_values(mag_V)))
    std = np.std(unp.nominal_values(mag_V))
    rel_mag = uncertainties.ufloat(rel_mag.nominal_value, std)
    print('mean relative magnitude: ', rel_mag)

    should_fix_extinction = True
    if should_fix_extinction:
        # See extinction correction from here
        # https://astronomy.swin.edu.au/cosmos/I/Interstellar+Reddening
        E_B_V = uncertainties.ufloat(0.1, 0.06)
        A_v = 3.2 * E_B_V

        # page 62 in book
        d = 10 ** ((rel_mag - abs_mag + 5 - A_v) / 5)
    else:
        d = 10**((rel_mag-abs_mag + 5) / 5)

    print("Distance to star is: ", d, 'pc')
    return d


def distance_gaia():
    distance_mas = uncertainties.ufloat(0.7541012207577649, 0.014584912)
    distance_as = distance_mas * 1e-3
    distance_pc = 1 / distance_as
    print("distance according to gaia:", distance_pc, "pc")


def main():
    # show_const_stars()
    # show_var_star(rel_photometry=True)
    # calc_period()
    # calc_period2(try_dense=False)
    # calc_abs_mag()
    calc_distance()
    # distance_gaia()


if __name__ == "__main__":
    main()
    plt.show()
