import matplotlib.pyplot as plt
import numpy as np
from uncertainties import ufloat
from uncertainties import unumpy as unp

import temperature
from parse_data import get_magnitudes


def calc_radius(T, V):
    T_s = 5772
    M_s = 4.83

    f1 = temperature.calc_expected_flux(T_s, temperature.V_filter_curve)
    f2 = temperature.calc_expected_flux(T, temperature.V_filter_curve)

    plt.show()

    # print(f1)
    # print(f2)

    a = np.sqrt(f1 / f2)

    b = unp.sqrt(100 ** ((M_s - V) / 5))

    R = a * b

    # print(f'Radius {R} in units of sun radius')
    return R


def plot_radius_vs_time():
    plt.rcParams['font.size'] = 14

    d = ufloat(1.58e+03, 0.26e+03)
    times_B, mag_B, times_V, mag_V = get_magnitudes()
    Vs = mag_V - 5 * unp.log10(d) + 5

    times, Ts, Ts_errs = temperature.analyze_temperature()

    Rs = np.zeros(len(Ts))
    Rs_min = np.zeros(len(Ts))
    Rs_max = np.zeros(len(Ts))

    for i, T in enumerate(Ts):
        Rs[i] = calc_radius(T, Vs[i]).nominal_value
        Rs_min[i] = calc_radius(Ts[i] - Ts_errs[0, i], Vs[i]).nominal_value
        Rs_max[i] = calc_radius(Ts[i] + Ts_errs[1, i], Vs[i]).nominal_value

    errs = np.zeros((2, len(Ts)))
    errs[0, :] = Rs - Rs_min
    errs[1, :] = Rs_max - Rs

    fig, ax = plt.subplots()
    ax.errorbar(times, Rs, errs, fmt='*-')
    # ax.plot(times, Rs, '-')

    ax.set_title('Radius vs time')
    ax.set_xlabel('time [hour]')
    ax.set_ylabel(r'Radius [$ R_\odot $]')

    fig.savefig("../Figures/radius.png")

    print(f'mean radius {Rs.mean()}')
    print(f'error  {Rs.std()}')

    plt.show()


def main():
    temperature.load_filter_curves()
    plot_radius_vs_time()
    # calc_radius(7700)


if __name__ == '__main__':
    main()
