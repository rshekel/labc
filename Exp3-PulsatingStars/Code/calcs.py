import numpy as np
import uncertainties


def calcs():
    a = uncertainties.ufloat(1830, 250)
    b = uncertainties.ufloat(1326, 26)
    print(calc_n_sigma(a, b))


    a = uncertainties.ufloat(1580, 260)
    b = uncertainties.ufloat(1326, 26)
    print(calc_n_sigma(a, b))


def calc_n_sigma(a: uncertainties.ufloat, b: uncertainties.ufloat):
    return abs((a.n - b.n) / np.sqrt(a.s ** 2 + b.s ** 2))


def main():
    calcs()


if __name__ == '__main__':
    main()