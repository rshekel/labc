#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language hebrew
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "David CLM"
\font_sans "default" "FreeSerif"
\font_typewriter "default" "FreeMono"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts true
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
ניסוי בכוכבים פועמים - תשובות לחלק
\family roman
\series medium
\shape up
\size largest
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
II
\end_layout

\begin_layout Author
רונן שקל ואלון סרדס
\end_layout

\begin_layout Section
טלסקופ עדשות וקרניים
\end_layout

\begin_layout Standard
כל הקרניים שמגיעות מכוכב אל כדור הארץ הן מקבילות, כי הכוכב נמצא רחוק מאוד
 ביחס לגודל הטלסקופ.
 ככל שהתצפית מתבצעת במקום רחוק יותר אז המפתח הזווית שנצפה הוא קטן יותר ולכן
 העקמומיות קטנה עם המרחק.
 ניתן לראות את האפקט הזה מתואר באיור למטה:
\end_layout

\begin_layout Standard
\begin_inset Graphics
	filename Figures/far-pane-wave.png
	width 70col%

\end_inset


\end_layout

\begin_layout Standard
קרניים שמגיעות 
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
בניצב לרכיב ה 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
CCD
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
יפגעו במרכז שלו.
 קרן שמגיעה בזווית ופוגעת במרכז העדשה לא משנה את הכיוון שלה, וקרניים אחרות
 מקבילות לה יתמקדו באותה הנקודה במישור המוקד.
 נצייר את הדרך שהקרניים עושות במעבר בעדשה:
\end_layout

\begin_layout Standard
\begin_inset Float figure
placement H
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename Figures/Rays.png
	width 80col%

\end_inset


\end_layout

\begin_layout Plain Layout
\begin_inset Caption Standard

\begin_layout Plain Layout
תרשים קרניים של עדשה
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
זווית הראייה
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
זה לא מה שרותם או יובל כתבו, אבל אני חושב שאני צודק.
 ראה גם 
\end_layout

\begin_layout Plain Layout

\lang english
https://www.skyatnightmagazine.com/astronomy-field-view-calculator/
\end_layout

\begin_layout Plain Layout
וגם
\end_layout

\begin_layout Plain Layout

\lang english
https://www.edmundoptics.com/knowledge-center/application-notes/imaging/understand
ing-focal-length-and-field-of-view/
\end_layout

\end_inset


\end_layout

\begin_layout Standard
נשתמש בסימונים שמוצגים בגרף של הקרניים.
 זווית הראייה )
\lang english
angular field of view
\lang hebrew
( של הטלסקופ נקבעת לפי הזווית המקסימלית 
\begin_inset Formula $\theta$
\end_inset

 שקרניים בזווית הזו עדיין יפגעו במצלמת
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
CCD
\lang hebrew
.
 משיקולים גיאומטריים, הקשר בין 
\begin_inset Formula $d$
\end_inset

 ל 
\begin_inset Formula $\theta$
\end_inset

 הוא
\begin_inset Formula 
\[
\frac{d}{f}=\tan\theta
\]

\end_inset

כש 
\begin_inset Formula $f$
\end_inset

 זה אורך המוקד של העדשה.
 נניח ש 
\begin_inset Formula $L$
\end_inset

 זה האורך של המצלמה.
 
\begin_inset Formula $d$
\end_inset

 המקסימלי עבור המצלמה הוא 
\begin_inset Formula $d_{\max}=\frac{L}{2}$
\end_inset

, כלומר
\begin_inset Formula 
\[
\frac{L}{2f}=\tan\theta_{\max}
\]

\end_inset

זה נותן לנו את המפתח הזוויתי המקסימלי ביחס לציר האופטי.
 המפתח הזוויתי של כל התמונה הוא 
\begin_inset Formula $2\theta_{\max}$
\end_inset

 כי הזוויות שאפשר לסרוק הן בטווח 
\begin_inset Formula $\left[-\theta_{\max},\theta_{\max}\right]$
\end_inset

.
 לכן, זווית הראייה 
\begin_inset Formula $\alpha=2\theta_{\max}$
\end_inset

 מקיימת את הקשר: 
\begin_inset Formula 
\[
\frac{L}{2f}=\tan\frac{\alpha}{2}
\]

\end_inset

נציב את הגדלים.
 המימדים של ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
CCD
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
במערכת שלנו הם: 
\begin_inset Formula $L_{x}=27.65\,\mathrm{mm},L_{y}=18.48\,\mathrm{mm}$
\end_inset

.
 נשתמש ב 
\begin_inset Formula $L=L_{y}$
\end_inset

 הקטן מביניהם.
 אורך המוקד הוא 
\begin_inset Formula $f=3454\,\mathrm{mm}$
\end_inset

.
 מזה נקבל:
\begin_inset Formula 
\[
\frac{L}{2f}=\tan\frac{\alpha}{2}\implies
\]

\end_inset


\begin_inset Formula 
\begin{align*}
\alpha & =2\arctan\left(\frac{L}{2f}\right)\\
 & =2\arctan\left(\frac{18.48}{2\cdot3454}\right)\\
 & =0.00535\,\mathrm{rad}\\
 & =0.00535\cdot\frac{360}{2\pi}\,\mathrm{degree}\\
 & =0.00535\cdot\frac{360}{2\pi}\cdot60\,\mathrm{arcmin}\\
 & =\boxed{18.4'}
\end{align*}

\end_inset

חישוב דומה עבור 
\begin_inset Formula $L=L_{x}$
\end_inset

 נותן זווית ראייה של 
\begin_inset Formula $\alpha=27.52'$
\end_inset


\end_layout

\begin_layout Section
זווית ראייה של פיקסל בודד
\end_layout

\begin_layout Standard
גודל פיקסל אצלנו הוא 
\begin_inset Formula $9\times9$
\end_inset

 בחידות של 
\begin_inset Formula $\mu m$
\end_inset

.
 בעזרת זה נוכל לחשב את זווית הראייה של כל פיקסל, עם הצבה 
\begin_inset Formula $L=0.009\,\mathrm{mm}$
\end_inset

:
\begin_inset Formula 
\begin{align*}
\alpha & =2\arctan\left(\frac{L}{2f}\right)\frac{360}{2\pi}\cdot60\,\mathrm{arcmin}\\
 & =2\arctan\left(\frac{0.009}{2\cdot3454}\right)\cdot\frac{360}{2\pi}\cdot60\,\mathrm{arcmin}\\
 & =0.00896'\\
 & =\boxed{0.54''}
\end{align*}

\end_inset

החישוב הזה נכון עבור הפיקסל שנמצא במרכז הקרן, כי רק תחת ההנחה הזו קיבלנו
 את הקשר בין הגודל 
\begin_inset Formula $L$
\end_inset

 ל 
\begin_inset Formula $\alpha$
\end_inset

 זווית הראייה.
 הניתוח של 
\begin_inset Formula $\frac{L}{2f}=\tan\frac{\alpha}{2}$
\end_inset

 התבסס על כך שהקרן שפוגעת במרכז המצלמה מגיעה בניצב למצלמה.
 נוכל להניח בקירוב שזו גם זווית הראייה של כל הפיקסלים האחרים.
 הקירוב הזה תקף רק בזוויות קטנות )כשגודל הזווית ברדיאנים קטן מ 
\begin_inset Formula $1$
\end_inset

( כי במקרה הזה יש קשר לינארי בין הזווית של הקרן 
\begin_inset Formula $\theta$
\end_inset

 למיקום בו היא תפגע במישור המוקד 
\begin_inset Formula $d$
\end_inset

.
 
\end_layout

\begin_layout Standard
נוכל להיווכח שהקירוב הזה נכון, אם נחשב את זווית הראייה הממוצעת של כל פיקסל:
 זווית הראייה של הטלסקופ בציר 
\begin_inset Formula $y$
\end_inset

 היא 
\begin_inset Formula $18.4'$
\end_inset

, ונוכל לחלק את זה במספר הפיקסלים כדי לקבל את אותה התוצאה 
\begin_inset Formula $\alpha=\frac{18.4}{2047}\cdot60''=0.54''$
\end_inset

.
 בגבול הזה עדיין 
\begin_inset Formula $\tan\theta\approx\theta$
\end_inset

 כי:
\begin_inset Formula 
\[
\tan\left(\frac{18.4}{60}\cdot\frac{2\pi}{360}\right)=\tan\left(0.00535\right)\approx0.00535
\]

\end_inset


\end_layout

\begin_layout Section
רזולוציה
\end_layout

\begin_layout Standard
מצאנו את הרזולוציה שגודל הפיקסל מכתיב לנו 
\begin_inset Formula $\alpha_{\text{pixel}}=0.54''$
\end_inset

.
 בנוסף לזה, יש את גבול הדיפרקציה שנובע מכך שמפתח הטלסקופ הוא בעל גודל סופי.
 בקורס בגלים ראינו שבשדה רחוק, גל מישורי שעובר דרך מפתח מעגלי מתאבך בצורה
 של
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
Airy Pattern
\lang hebrew
 )צורה של דיסק ראשי וטבעות(.
 התמונה שמתקבלת בשדה רחוק היא גם התמונה שמתקבלת במישור המוקד של העדשה.
 כל כוכב נמצא באינסוף, וניתן להתייחס אליו כמקור לגל מישורי.
 לכן האור שמגיע מכוכב ונופל על המישור האופטי מראה תבנית התאבכות של 
\lang english
Airy Pattern
\lang hebrew
.
 קריטריון ריילי )
\lang english
Rayleigh
\lang hebrew
( קובע ששני מקורות עדיין ניתנים לזיהוי אם המרכז של הדיסק הראשי של אחד מהם
 נופל על המינימום )החושך( הראשון של תבנית ההתאבכות של המקור השני.
\end_layout

\begin_layout Standard
לפי הספר 
\begin_inset CommandInset citation
LatexCommand cite
key "carroll_ostlie_2017"
literal "false"

\end_inset


\begin_inset Note Note
status open

\begin_layout Plain Layout
עמוד
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\numeric on
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
145
\end_layout

\end_inset

, המפתח הזוויתי המינימלי בין שני מקורות כך שהם יהיו מובחנים אחד מהשני הוא
\begin_inset Formula 
\[
\alpha_{\text{diffraction}}=1.22\frac{\lambda}{D}
\]

\end_inset

כש 
\begin_inset Formula $\lambda$
\end_inset

 זה אורך הגל ו 
\begin_inset Formula $D$
\end_inset

 זה המפתח של הטלסקופ.
 נשתמש באורך גל של אור כחול 
\begin_inset Formula $\lambda=400\,\mathrm{nm}$
\end_inset

, ובטלסקופ שלנו 
\begin_inset Formula $D=508\,\mathrm{mm}$
\end_inset

 נציב ונקבל:
\begin_inset Formula 
\begin{align*}
\alpha_{\text{diffraction}} & =1.22\frac{400\times10^{-9}}{508\times10^{-3}}\\
 & =9.6\times10^{-7}\,\mathrm{rad}\\
 & =9.6\times10^{-7}\cdot\frac{360}{2\pi}\cdot60\cdot60\,\mathrm{arcsec}\\
 & =0.2''
\end{align*}

\end_inset

במקרה הזה אנחנו רואים ש 
\begin_inset Formula $\alpha_{\text{diffraction}}<\alpha_{\text{pixel}}$
\end_inset

 כלומר, הרזולוציה של הטלסקופ נקבעת לפי זווית הראייה של כל פיקסל.
\end_layout

\begin_layout Standard
יש עוד מקור לרזולוציה נמוכה והוא הראות )
\lang english
seeing
\lang hebrew
(.
 כאשר קרני האור עוברות דרך האטמוספרה, חזית הגל מתעוותת.
 זה קורה בגלל שהאטמוספרה לא אחידה, יש אזורים יותר צפופים ופחות צפופים וזה
 משפיע על מקדם השבירה )
\lang english
refractive index
\lang hebrew
(, מה שגורם לחלקים שונים בחזית הגל לצבור פאזה לא אחידה.
 ראות נמוכה גורמת לכוכבים עצמם להימרח על זווית מרחבית.
 במיקומים הכי טובים בעולם מגיעים לרזולוציה של 
\begin_inset Formula $\alpha\approx0.5''$
\end_inset

, במקומות מגיעים ל 
\begin_inset Formula $\alpha\approx2''$
\end_inset

, אנחנו מעריכים שאצלנו תהיה רזולוציה באזור של 
\begin_inset Formula $\alpha_{\text{seeing}}\approx2.5''$
\end_inset


\begin_inset Note Note
status collapsed

\begin_layout Plain Layout
לפי 
\lang english
http://slittlefair.staff.shef.ac.uk/teaching/phy241/lectures/L01/index.html
\end_layout

\end_inset

, ונראה שזה הגורם המשמעותי שמגביל את הרזולוציה שלנו.
\end_layout

\begin_layout Section
הגודל של כוכב
\end_layout

\begin_layout Standard
\begin_inset Note Note
status open

\begin_layout Plain Layout
לא ברור לי מה זה זווית מרחבית על ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
CCD
\lang hebrew
...
\end_layout

\end_inset

ניקח את הכוכב
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
Alpha Centauri
\lang hebrew
, כוכב שנמצא קרוב למערכת השמש.
 המרחק שלו מכדור הארץ הוא 
\begin_inset Formula 
\[
d=1.34\,\mathrm{pc}=4.1348\times10^{16}\,\mathrm{m}
\]

\end_inset

הרדיוס שלו הוא 
\begin_inset Formula 
\[
R=1.223R_{\astrosun}=8.508\times10^{8}\,\mathrm{m}
\]

\end_inset

הזווית המרחבית 
\begin_inset Formula $\theta$
\end_inset

 של הכוכב הזה תהיה:
\begin_inset Formula 
\[
\tan\frac{\theta}{2}=\frac{R}{d}\implies
\]

\end_inset


\begin_inset Formula 
\begin{align*}
\theta & \approx2\frac{R}{d}\\
 & \approx2\frac{8.508\times10^{8}}{4.1348\times10^{16}}\\
 & \approx4\times10^{-8}\,\mathrm{rad}\\
 & \approx4\times10^{-8}\cdot\frac{360}{2\pi}\cdot60\cdot60\,\mathrm{arcsec}\\
 & \approx0.008''
\end{align*}

\end_inset

זה גודל קטן מאוד, קטן מהרזולוציה שלנו.
 בפועל נצפה שבגלל הראות )
\lang english
seeing
\lang hebrew
( הכוכב יימרח על כמה פיקסלים.
 ראינו שראות גורמת לזווית של 
\begin_inset Formula $\alpha_{\text{seeing}}\approx2.5''$
\end_inset

 לעומת הזווית של פיקסל 
\begin_inset Formula $\alpha_{\text{pixel}}\approx0.5''$
\end_inset

, כלומר כוכב יימרח על כ 
\begin_inset Formula $5\times5$
\end_inset

 פיקסלים.
\end_layout

\begin_layout Section
רעש
\end_layout

\begin_layout Standard
ננתח את הרעש שיש במדידות כדי להעריך את ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\lang hebrew
.
 כל אובייקט שפולט אור פולט פוטונים בתהליך רנדומלי, עם התפלגות פואסונית.
 בהתפלגות פואסונית עם תוחלת 
\begin_inset Formula $N$
\end_inset

 סטיית התקן היא 
\begin_inset Formula $\sqrt{N}$
\end_inset

.
 לכן אם כמות הפוטונים לשנייה שנפלטת מעצם כלשהו היא 
\begin_inset Formula $N$
\end_inset

, הרעש מתנהג לפי 
\begin_inset Formula $\sqrt{N}$
\end_inset

.
 רעש מהסוג הזה נקרא
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
shot noise
\lang hebrew
.
 נשתמש בעיקרון הזה כדי להבין את הרעש.
\end_layout

\begin_layout Standard
יש מספר מקורות שתורמים לסיגנל הכולל שנמדד ממצלמת ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
CCD
\lang hebrew
, וכל אחד תורם רעש:
\end_layout

\begin_layout Standard

\series bold
הכוכב:
\series default
 נסמן את כמות הפוטונים שמגיעים מהכוכב לטלסקופ בזמן החשיפה ב 
\begin_inset Formula $S_{o}$
\end_inset

, והרעש הוא 
\begin_inset Formula $\sqrt{S_{o}}$
\end_inset

.
\end_layout

\begin_layout Standard

\series bold
רעש רקע: 
\series default
חוץ מהכוכב, הטלסקופ מקבל פוטונים מעצמים אחרים )ירח או אור מהקרקע( שמתפזרים
 באטמוספרה.
 אור כזה נקרא
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
רקע שמיים )
\lang english
sky background
\lang hebrew
(.
 נסמן את כמות הפוטונים מרקע השמיים ב 
\begin_inset Formula $S_{s}$
\end_inset

, וגם כאן הרעש הוא 
\begin_inset Formula $\sqrt{S_{s}}$
\end_inset

.
 כדי לזהות את העוצמה של רקע השמיים 
\begin_inset Formula $S_{s}$
\end_inset

 צריך להסתכל טבעת מסביב לכוכב אותו אנחנו מודדים.
\end_layout

\begin_layout Standard

\series bold
אקסיטציה תרמית: 
\series default
המנגנון שעליו מבוסס ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
CCD
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
הוא ספירת האלקטרונים שמעוררים לפס ההולכה במוליך למחצה.
 אם לפוטון יש מספיק אנרגיה הוא יכול לגרום לאלקטרון לעלות לפס ההולכה, אך
 זה לא התהליך היחיד שגורם לאלקטרון לעבור לפס הולכה.
 אקסיטציה תרמית היא עוד תהליך שגורם לאלקטרון לעבור לפס ההולכה ואז להיספר
 כמו פוטון.
 גם אקסיטציה תרמית היא תהליך אקראי, שמתפלגת פואסונית, ולכן אם כמות האלקטרונים
 שנספרים בגלל אקסיטציה תרמית הוא 
\begin_inset Formula $S_{d}$
\end_inset

 )
\lang english
d - dark count
\lang hebrew
( אז הרעש הוא 
\begin_inset Formula $\sqrt{S_{d}}$
\end_inset

.
 כדי לזהות את הסיגנל 
\begin_inset Formula $S_{d}$
\end_inset

 ניתן לקחת תמונה עם צמצם סגור, כלומר בלי אור שמגיע למצלמה, ואז כל הפוטונים
 שנמדדים הם כתוצאה מאקסיטציה תרמית.
\end_layout

\begin_layout Standard

\series bold
רעש קריאה: 
\series default
באנגלית
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
readout noise
\lang hebrew
.
 האלקטרונים שעברו לפס ההולכה הם פרופורציונים לכמות הפוטונים שנקלטו.
 לאחר שנגמר זמן החשיפה יש צורך למדוד את כמות האלקטרונים המעוררים כדי לקבל
 את כמות הפוטונים.
 בתהליך הזה של ספירת כמות האלקטרונים יש טעות קריאה.
 אם יש 
\begin_inset Formula $n$
\end_inset

 אלקטרונים מעוררים, כמות האלקטרונים המעוררים שיימדדו יתפלג גאוסייאנית מסביב
 ל 
\begin_inset Formula $n$
\end_inset

 עם סטיית תקן 
\begin_inset Formula $R$
\end_inset

.
 הרעש במקרה הזה הוא 
\begin_inset Formula $R$
\end_inset

 והוא לא תלוי בזמן החשיפה או בכמות הפוטונים הכוללת שנמדדת.
\end_layout

\begin_layout Standard
כל התהליכים האלה הם בלתי תלויים ולכן השונות של הסכום הוא סכום השונות של
 כל אחד.
 לכן הרעש הכולל 
\begin_inset Formula $N$
\end_inset

 הוא
\begin_inset Formula 
\[
N^{2}=S_{0}+S_{s}+S_{d}+R^{2}
\]

\end_inset


\begin_inset Formula 
\[
N=\sqrt{S_{0}+S_{s}+S_{d}+R^{2}}
\]

\end_inset

הסיגנל שלנו הוא האור שנפלט מהאובייקט 
\begin_inset Formula $S_{o}$
\end_inset

 ולכן ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
במדידה הוא
\begin_inset Formula 
\[
\boxed{\text{SNR}=\frac{S_{o}}{\sqrt{S_{0}+S_{s}+S_{d}+R^{2}}}}
\]

\end_inset


\end_layout

\begin_layout Standard
נוכל לקרב את ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
למקרים שבהם הסיגנל מהאובייקט גדול ביחס לסיגנלים הלא רצויים )
\lang english
object dominated
\lang hebrew
(, הרעש הדומיננטי נובע מהרעש של האובייקט 
\begin_inset Formula $S_{o}$
\end_inset

.
 במקרה הזה
\begin_inset Formula 
\[
\text{SNR}=\frac{S_{o}}{\sqrt{S_{o}}}=\sqrt{S_{o}}
\]

\end_inset

כמות הפוטונים שנקלטים 
\begin_inset Formula $S_{o}$
\end_inset

 פרופורציונית לזמן החשיפה, לכן נקבל
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
טוב יותר אם נגדיל את זמן החשיפה.
 ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
גדל פי שורש זמן החשיפה.
 בנוסף, כמות האור שנקלטת מהאובייקט פרופורציונית ל 
\begin_inset Formula $D^{2}$
\end_inset

 כש 
\begin_inset Formula $D$
\end_inset

 זה גודל המפתח של הטלסקופ.
 לכן ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
במקרה הזה גדל לינארית עם גודל המפתח.
\end_layout

\begin_layout Standard
במצב אחר, כשהסיגנל מהאובייקט חלש אבל רקע השמיים חזק )
\lang english
sky dominated
\lang hebrew
( נקרב את הרעש לפי 
\begin_inset Formula $S_{s}\gg S_{o},S_{d},R^{2}$
\end_inset

 ואז
\begin_inset Formula 
\[
\text{SNR}=\frac{S_{o}}{\sqrt{S_{s}}}
\]

\end_inset

גם במקרה הזה, 
\begin_inset Formula $S_{o}$
\end_inset

 ו 
\begin_inset Formula $S_{s}$
\end_inset

 גדלים לינארית עם זמן החשיפה ולכן זמן חשיפה ארוך יותר יגדיל את ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
במדידה, גם לפי תלות של שורש זמן החשיפה.
 שיקול דומה למקודם, שני הגדלים 
\begin_inset Formula $S_{o}$
\end_inset

 ו 
\begin_inset Formula $S_{s}$
\end_inset

 משתנים פרופורציונית ל 
\begin_inset Formula $D^{2}$
\end_inset

 ולכן גם כאן ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
גדל לינארית עם גודל המפתח.
 דרך נוספת להגדיל את ה
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
\lang english
SNR
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
\lang hebrew
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\xout default
\uuline default
\uwave default
\noun default
\color inherit
במקרה הזה היא להקטין את 
\begin_inset Formula $S_{s}$
\end_inset

 כלומר את האור מרקע השמיים.
 ניתן לעשות את זה על ידי בחירת זמן מתאים למדידה, למשל כשאין ירח.
\end_layout

\begin_layout Section
חילוץ הטמפרטורה על ידי מדידה בפילטרים שונים
\end_layout

\begin_layout Standard
נניח שאנחנו מודדים את השטף שמגיע מהכוכב דרך שני פילטרים שונים שמעבירים רק
 חלק מאורכי הגל.
 נרצה למצוא באמצעות מדידה כזו את הטמפרטורה של הכוכב.
 נתייחס אל הכוכב כגוף שחור ונאפיין את שטף הקרינה שמגיע לכדור הארץ תחת פילטר
 מסוים.
 נסמן את פונקציית ההעברה של הפילטר ב 
\begin_inset Formula $f\left(\lambda\right)$
\end_inset

.
 בתור פונקציית העברה, שטף האור שמגיע מהכוכב באורך גל 
\begin_inset Formula $\lambda$
\end_inset

 היא 
\begin_inset Formula $F\left(\lambda\right)f\left(\lambda\right)$
\end_inset

, כלומר 
\begin_inset Formula $f\left(\lambda\right)=0$
\end_inset

 מתאים לסינון מלא של הקרן ו 
\begin_inset Formula $f\left(\lambda\right)=1$
\end_inset

 מתאים להעברה מלאה של הקרן.
\end_layout

\begin_layout Standard
כדי לקבל את השטף מהכוכב תחת הפילטר 
\begin_inset Formula $f\left(\lambda\right)$
\end_inset

 נשתמש בפונקציית פלאנק )ביחידות של אורך הגל 
\begin_inset Formula $\lambda$
\end_inset

( ונמצא את הבהירות של הכוכב 
\begin_inset Formula $L_{f}$
\end_inset

.
 פונקציית פלאנק היא
\begin_inset Formula 
\[
B_{\lambda}\left(T\right)=\frac{\nicefrac{2hc^{2}}{\lambda^{5}}}{\exp\left(\frac{hc}{\lambda k_{B}T}\right)-1}
\]

\end_inset

הבהירות הכוללת שמתקבלת מהכוכב תתקבל מאינטגרל על כל שטח הפנים של הכוכב, על
 זווית מרחבית של חצי המיספירה ועל כל אורכי הגל:
\begin_inset Formula 
\begin{align*}
L_{f} & =B_{\lambda}\left(T\right)\cdot f\left(\lambda\right)\cdot d\lambda dA\cos\theta d\Omega\\
 & =\left[\int_{0}^{\infty}B_{\lambda}\left(T\right)\cdot f\left(\lambda\right)\cdot d\lambda\right]\left[\int dA\right]\left[\int\cos\theta d\Omega\right]
\end{align*}

\end_inset

השטף שמגיע לכדור הארץ פרופורציוני ל 
\begin_inset Formula $L$
\end_inset

 אבל דועך לפי 
\begin_inset Formula $r^{2}$
\end_inset

 המרחק של הכוכב מכדור הארץ:
\begin_inset Formula 
\[
F_{f}=\frac{L_{f}}{4\pi r^{2}}
\]

\end_inset

אם ניקח מדידות משני פילטרים 
\begin_inset Formula $f_{1},f_{2}$
\end_inset

, ונחלק את השטף שנמדד מכל אחד מהם, לאחר צמצום איברים נישאר עם היחס:
\begin_inset Formula 
\[
\boxed{\frac{F_{f_{1}}}{F_{f_{2}}}=\frac{\int_{0}^{\infty}B_{\lambda}\left(T\right)\cdot f_{1}\left(\lambda\right)\cdot d\lambda}{\int_{0}^{\infty}B_{\lambda}\left(T\right)\cdot f_{2}\left(\lambda\right)\cdot d\lambda}}
\]

\end_inset

צד שמאל במשוואה ידוע )הוא נמדד(.
 התלות הפונקציונלית 
\begin_inset Formula $B_{\lambda}\left(T\right)$
\end_inset

 ידועה, ו 
\begin_inset Formula $f_{1},f_{2}$
\end_inset

 הן פונקציות המעבר של הפילטרים.
 מקבלים כאן משוואה סתומה עם נעלם יחיד 
\begin_inset Formula $T$
\end_inset

.
 בעזרת כלים נומריים ניתן למצוא את 
\begin_inset Formula $T$
\end_inset

 הטמפרטורה שפותרת את המשוואה.
 זאת הטמפרטורה של הכוכב.
\end_layout

\begin_layout Standard

\lang english
\begin_inset CommandInset bibtex
LatexCommand bibtex
btprint "btPrintCited"
bibfiles "references"
options "plain"

\end_inset


\end_layout

\end_body
\end_document
