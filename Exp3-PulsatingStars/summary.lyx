#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "FreeSans"
\font_sans "default" "FreeSerif"
\font_typewriter "default" "FreeMono"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Black body radiation
\end_layout

\begin_layout Standard
Wien's displacement law is the relation between the wavelength where max
 radiation occurs and the temperature:
\begin_inset Formula 
\[
\lambda_{\max}T=0.002897755\,mK
\]

\end_inset

 
\end_layout

\begin_layout Standard
The luminosity of a star (I guess this is radiation per time, with units
 of power) is
\begin_inset Formula 
\[
L=4\pi R^{2}\sigma T_{e}^{4}
\]

\end_inset


\begin_inset Formula $R$
\end_inset

 the radius of the star, 
\begin_inset Formula $\sigma$
\end_inset

 is the Stefan-Boltzmann constant, 
\begin_inset Formula $T_{e}$
\end_inset

 is the 
\begin_inset Quotes eld
\end_inset

temperature
\begin_inset Quotes erd
\end_inset

 (in fact this is defined as the effective temperature.
 I guess that 
\begin_inset Formula $L$
\end_inset

 and 
\begin_inset Formula $R$
\end_inset

 are known)
\end_layout

\begin_layout Subsection
\begin_inset space ~
\end_inset


\end_layout

\begin_layout Standard
According to classical physics, the energy of a classic harmonic oscillator
 with frequency 
\begin_inset Formula $\omega$
\end_inset

 at temperature 
\begin_inset Formula $T$
\end_inset

 is
\begin_inset Formula 
\begin{align*}
Z & =\frac{1}{\hbar}\int dxdpe^{-\beta\left(\frac{p^{2}}{2m}+\frac{k}{2}x^{2}\right)}\\
\underbrace{*}_{\omega=\sqrt{\frac{k}{m}}} & =\frac{1}{\hbar}\int dxdpe^{-\beta\left(\frac{p^{2}}{2m}+\frac{m\omega^{2}}{2}x^{2}\right)}\\
 & =\frac{1}{\hbar}\left[\int dxe^{-\beta\frac{m\omega^{2}}{2}x^{2}}\right]\left[\int e^{-\beta\frac{p^{2}}{2m}}dp\right]\\
 & =\frac{1}{\hbar}\sqrt{\frac{\pi}{\beta\frac{m\omega^{2}}{2}}}\sqrt{\frac{\pi}{\beta\frac{1}{2m}}}\\
 & =\frac{2\pi}{\hbar}\sqrt{\frac{1}{\beta\omega^{2}}}\sqrt{\frac{1}{\beta}}\\
 & =\frac{2\pi}{\hbar}\frac{1}{\omega}k_{B}T\\
 & =\frac{2\pi}{\hbar\omega\beta}
\end{align*}

\end_inset

and the energy
\begin_inset Formula 
\begin{align*}
U & =-\frac{\partial}{\partial\beta}\ln z\\
 & =-\frac{\partial}{\partial\beta}\ln\left(\frac{2\pi}{\hbar\omega\beta}\right)\\
 & =-\frac{\partial}{\partial\beta}\ln\left(\frac{1}{\beta}\right)\\
 & =\frac{\partial}{\partial\beta}\ln\left(\beta\right)\\
 & =\frac{1}{\beta}\\
 & =k_{B}T
\end{align*}

\end_inset

which is independent on 
\begin_inset Formula $\omega$
\end_inset

.
 This is a catastrophe!
\end_layout

\begin_layout Standard
Let us redo the Rayleigh's derivation.
 The radiation is:
\begin_inset Formula 
\[
g\left(\lambda\right)d\lambda U\left(\lambda\right)
\]

\end_inset

with 
\begin_inset Formula $g\left(\lambda\right)$
\end_inset

 density of states (how much different modes we have with a given 
\begin_inset Formula $\lambda$
\end_inset

.
 We know that 
\begin_inset Formula 
\begin{align*}
\omega & =ck\\
 & =c\sqrt{k_{x}^{2}+k_{y}^{2}+k_{z}^{2}}\\
 & =\pi c\sqrt{\left(\frac{n_{x}}{L}\right)^{2}+\left(\frac{n_{y}}{L}\right)^{2}+\left(\frac{n_{z}}{L}\right)^{2}}\\
 & =\frac{\pi c}{L}\sqrt{\left(n_{x}\right)^{2}+\left(n_{y}\right)^{2}+\left(n_{z}\right)^{2}}
\end{align*}

\end_inset


\begin_inset Formula 
\[
\sqrt{n_{x}^{2}+n_{y}^{2}+n_{z}^{2}}\leq\frac{\omega L}{\pi c}
\]

\end_inset

The volume is 
\begin_inset Formula 
\[
\Gamma\left(\omega\right)=\frac{4\pi}{3}\left(\frac{\omega L}{\pi c}\right)^{3}
\]

\end_inset

Taking 
\begin_inset Formula $2$
\end_inset

 modes for polarization, but actually only 
\begin_inset Formula $1/8$
\end_inset

 of the ball:
\begin_inset Formula 
\[
\Gamma\left(\omega\right)=\frac{\pi}{3}\left(\frac{\omega L}{\pi c}\right)^{3}
\]

\end_inset

Taking derivative:
\begin_inset Formula 
\begin{align*}
g\left(\omega\right)d\omega & =\frac{d\Gamma}{d\omega}d\omega\\
 & =\pi\left(\frac{L}{\pi c}\right)^{3}\omega^{2}d\omega\\
 & =\frac{1}{\pi^{2}}\left(\frac{L}{c}\right)^{3}\omega^{2}d\omega
\end{align*}

\end_inset


\begin_inset Note Note
status open

\begin_layout Plain Layout
This seems correct, comparing to the results from 77307 Mechanical Statistics,
 TA11
\end_layout

\end_inset


\begin_inset Formula 
\[
\omega=ck=\frac{2\pi c}{\lambda}
\]

\end_inset


\begin_inset Formula 
\[
d\omega=-\frac{2\pi c}{\lambda^{2}}d\lambda
\]

\end_inset


\begin_inset Formula 
\begin{align*}
g\left(\lambda\right)d\lambda & =\pi\left(\frac{L}{\pi c}\right)^{3}\left(\frac{2\pi c}{\lambda}\right)^{2}\left(\frac{2\pi c}{\lambda^{2}}\right)d\lambda\\
 & =\frac{8\pi}{\lambda^{4}}L^{3}d\lambda
\end{align*}

\end_inset


\end_layout

\begin_layout Standard
multiply by 
\begin_inset Formula $c$
\end_inset

 gives the flux of energy.
\begin_inset Formula 
\[
\propto\frac{8\pi}{\lambda^{4}}k_{B}T
\]

\end_inset


\end_layout

\begin_layout Standard
The correction is as follows:
\begin_inset Formula 
\begin{align*}
Z & =\frac{1}{\hbar}\sum e^{-\beta\left(n+\frac{1}{2}\right)\hbar\omega}\\
 & =\frac{1}{\hbar}e^{-\frac{\beta\hbar\omega}{2}}\sum e^{-\beta n\hbar\omega}\\
 & =\frac{1}{\hbar}\frac{e^{-\frac{\beta\hbar\omega}{2}}}{1-e^{-\beta\hbar\omega}}
\end{align*}

\end_inset


\begin_inset Formula 
\begin{align*}
U & =-\frac{\partial}{\partial\beta}\ln z\\
 & =-\frac{\partial}{\partial\beta}\ln\left(\frac{e^{-\frac{\beta\hbar\omega}{2}}}{1-e^{-\beta\hbar\omega}}\right)\\
\text{computer} & =\frac{\hbar\omega}{2\tanh\left(\frac{\beta\hbar\omega}{2}\right)}\\
 & \approx\frac{\hbar\omega}{2}\left(\frac{\exp\left(\frac{\beta\hbar\omega}{2}\right)+\exp\left(-\frac{\beta\hbar\omega}{2}\right)}{\exp\left(\frac{\beta\hbar\omega}{2}\right)-\exp\left(-\frac{\beta\hbar\omega}{2}\right)}\right)\\
 & \approx\frac{\hbar\omega}{2}\left(\frac{\exp\left(\beta\hbar\omega\right)+1}{\exp\left(\beta\hbar\omega\right)-1}\right)\\
 & \approx\frac{\hbar\omega}{\exp\left(\beta\hbar\omega\right)-1}
\end{align*}

\end_inset


\end_layout

\begin_layout Subsection
Planck's equation:
\end_layout

\begin_layout Standard
The energy of a single 
\begin_inset Formula $\omega$
\end_inset

 is 
\begin_inset Formula $E\left(\omega\right)=\frac{\hbar\omega}{\exp\left(\beta\hbar\omega\right)-1}$
\end_inset

.
 Changing 
\begin_inset Formula $\omega$
\end_inset

 to the frequency 
\begin_inset Formula $\nu$
\end_inset

:
\begin_inset Formula 
\[
E\left(\nu\right)=\frac{h\nu}{\exp\left(\beta h\nu\right)-1}
\]

\end_inset

The energy per 
\begin_inset Formula $\nu$
\end_inset

 becomes:
\begin_inset Formula 
\begin{align*}
g\left(\nu\right)E\left(\nu\right)d\nu & =E\left(\nu\right)\pi\left(\frac{L}{\pi c}\right)^{3}\left(2\pi\nu\right)^{2}d\nu\cdot2\pi\\
 & =E\left(\nu\right)8\pi\left(\frac{L}{c}\right)^{3}\nu^{2}d\nu\\
 & =\frac{h\nu}{\exp\left(\beta h\nu\right)-1}8\pi\left(\frac{L}{c}\right)^{3}\nu^{2}d\nu\\
 & =\frac{h\nu^{3}}{\exp\left(\beta h\nu\right)-1}8\pi\left(\frac{L}{c}\right)^{3}d\nu
\end{align*}

\end_inset

This is the energy inside the black body.
 The energy per unit volume is
\begin_inset Formula 
\[
u\left(\nu\right)d\nu=\frac{h\nu^{3}}{\exp\left(\beta h\nu\right)-1}8\pi\left(\frac{1}{c}\right)^{3}d\nu
\]

\end_inset


\end_layout

\begin_layout Standard
See Mechanical Statistics, TA11.
 If we have a small 
\begin_inset Formula $dA$
\end_inset

 gap in the black body surface, what is the flux of energy coming out of
 it? We need to look only at photons whose distance from 
\begin_inset Formula $dA$
\end_inset

 is shorter than 
\begin_inset Formula $ldt$
\end_inset

.
 Each photon, with angle 
\begin_inset Formula $\theta$
\end_inset

 between the line perpendicular to 
\begin_inset Formula $dA$
\end_inset

 has the probability to pass through 
\begin_inset Formula $dA$
\end_inset

 given by 
\begin_inset Formula $\frac{dA\cos\theta}{4\pi l^{2}}$
\end_inset

.
 The total of energy (prop to photons) that passes through 
\begin_inset Formula $dA$
\end_inset

 at time 
\begin_inset Formula $dt$
\end_inset

 is:
\begin_inset Formula 
\begin{align*}
f & =\int_{0}^{2\pi}\int_{0}^{\pi/2}\int_{0}^{cdt}\frac{dA\cos\theta}{4\pi l^{2}}\cdot l^{2}\sin\theta dld\theta d\phi\\
 & =\frac{dA}{4\pi}\int_{0}^{2\pi}\int_{0}^{\pi/2}\int_{0}^{cdt}\cos\theta\sin\theta dld\theta d\phi\\
 & =\frac{dA}{2}\int_{0}^{\pi/2}\int_{0}^{cdt}\cos\theta\sin\theta dld\theta\\
 & =\frac{dA}{2}\int_{0}^{\pi/2}\cos\theta\sin\theta d\theta\\
 & =\frac{dAcdt}{2}\cdot\frac{1}{2}\\
 & =\frac{dAcdt}{4}
\end{align*}

\end_inset

This is the relevant volume the energy comes out of and passes through 
\begin_inset Formula $dA$
\end_inset

, coming out in all direction.
 In fact, we only need the energy coming out in 
\begin_inset Formula $d\Omega$
\end_inset

, the relevant volume becomes:
\begin_inset Formula 
\begin{align*}
f\left(d\Omega\right) & =\int_{0}^{cdt}\int_{d\Omega}\frac{dA\cos\theta}{4\pi l^{2}}\cdot l^{2}dld\Omega\\
 & =\frac{dAcdt}{4\pi}\int_{d\Omega}\cos\theta d\Omega
\end{align*}

\end_inset

And the energy becomes:
\begin_inset Formula 
\begin{align*}
F\left(\nu\right)d\nu & =\frac{h\nu^{3}}{\exp\left(\beta h\nu\right)-1}8\pi\left(\frac{1}{c}\right)^{3}d\nu\left(\frac{dAcdt}{4\pi}\int_{d\Omega}\cos\theta d\Omega\right)\\
 & =\underbrace{\frac{2}{c^{2}}\frac{h\nu^{3}}{\exp\left(\beta h\nu\right)-1}}_{B_{\nu}\left(T\right)}d\nu dAdt\left(\int_{d\Omega}\cos\theta d\Omega\right)
\end{align*}

\end_inset

The function
\begin_inset Formula 
\[
B_{\nu}\left(T\right)=\frac{2}{c^{2}}\frac{h\nu^{3}}{\exp\left(\beta h\nu\right)-1}
\]

\end_inset

is called Planck's equation, and as we can see, it describes the energy
 flux the exits the black body per unit frequency, per unit solid angle.
\end_layout

\begin_layout Subsection
More
\end_layout

\begin_layout Standard
They say (in Rotem Ovdadia for example, and Yuval too, and statistical mechanics
 too) that 
\begin_inset Formula $P=\frac{1}{3}u$
\end_inset

 for ultra relative particles, 
\begin_inset Formula $P$
\end_inset

 the pressure, and 
\begin_inset Formula $u$
\end_inset

 the internal energy (per volume).
 Let's try to derive that.
 ultra relative, means 
\begin_inset Formula $m=0$
\end_inset


\begin_inset Formula 
\[
E=pc
\]

\end_inset


\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{align*}
Z & =\frac{V}{\hbar}\int_{0}^{\infty}e^{-\beta pc}dp\\
 & =\frac{V}{\hbar}\int_{0}^{\infty}e^{-\beta pc}dp\\
 & =\frac{V}{\hbar}\frac{1}{\beta c}
\end{align*}

\end_inset

See my notes for statistical mechanics:
\begin_inset Formula 
\begin{align*}
F & =-k_{B}T\ln Z\\
 & =-k_{B}T\ln\left(\frac{V}{\hbar}\frac{1}{\beta c}\right)
\end{align*}

\end_inset


\begin_inset Formula 
\begin{align*}
P & =-\left(\frac{\partial F}{\partial V}\right)_{T,N}\\
 & =\left(\frac{\partial}{\partial V}k_{B}T\ln\left(\frac{V}{\hbar}\frac{1}{\beta c}\right)\right)_{T,N}\\
 & =\left(\frac{\partial}{\partial V}k_{B}T\ln\left(V\right)\right)_{T,N}\\
 & =\frac{k_{B}T}{V}
\end{align*}

\end_inset

we got 
\begin_inset Formula $PV=k_{B}T$
\end_inset

...
 what is the internal energy?
\begin_inset Formula 
\begin{align*}
U & =-\frac{\partial}{\partial\beta}\ln z\\
 & =-\frac{\partial}{\partial\beta}\ln\left(\frac{V}{\hbar}\frac{1}{\beta c}\right)\\
 & =-\frac{\partial}{\partial\beta}\ln\left(\frac{1}{\beta}\right)\\
 & =\frac{\partial}{\partial\beta}\ln\left(\beta\right)\\
 & =\frac{1}{\beta}\\
 & =k_{B}T
\end{align*}

\end_inset

This is not good...
\end_layout

\begin_layout Standard
I think that what I did is for 1D case.
 In 3D world,
\begin_inset Formula 
\begin{align*}
Z & =4\pi\frac{V}{\hbar^{3}}\int_{0}^{\infty}p^{2}e^{-\beta pc}dp\\
 & =4\pi\frac{V}{\hbar^{3}}\frac{2}{\beta^{3}c^{3}}\\
 & =\frac{V}{\hbar^{3}}\frac{8\pi}{\beta^{3}c^{3}}
\end{align*}

\end_inset

Better I think...
 Let's continue.
\begin_inset Formula 
\begin{align*}
F & =-k_{B}T\ln Z\\
 & =-k_{B}T\ln\left(\frac{V}{\hbar^{3}}\frac{8\pi}{\beta^{3}c^{3}}\right)
\end{align*}

\end_inset


\begin_inset Formula 
\begin{align*}
P & =-\left(\frac{\partial F}{\partial V}\right)_{T,N}\\
 & =\left(\frac{\partial}{\partial V}k_{B}T\ln\left(\frac{V}{\hbar^{3}}\frac{8\pi}{\beta^{3}c^{3}}\right)\right)_{T,N}\\
 & =\left(\frac{\partial}{\partial V}k_{B}T\ln\left(V\right)\right)_{T,N}\\
 & =\frac{k_{B}T}{V}
\end{align*}

\end_inset

Again, 
\begin_inset Formula $PV=k_{B}T$
\end_inset


\begin_inset Formula 
\begin{align*}
U & =-\frac{\partial}{\partial\beta}\ln z\\
 & =-\frac{\partial}{\partial\beta}\ln\left(\frac{V}{\hbar^{3}}\frac{8\pi}{\beta^{3}c^{3}}\right)\\
 & =-\frac{\partial}{\partial\beta}\ln\left(\frac{1}{\beta^{3}}\right)\\
 & =\frac{\partial}{\partial\beta}\ln\left(\beta^{3}\right)\\
 & =3\frac{\partial}{\partial\beta}\ln\left(\beta\right)\\
 & =3k_{B}T
\end{align*}

\end_inset

and now we see that 
\begin_inset Formula $\frac{u}{3}=k_{B}T$
\end_inset

 and as expected:
\begin_inset Formula 
\[
P=\frac{u}{3}
\]

\end_inset

(nice...)
\end_layout

\begin_layout Section
Celestial Coordinates
\end_layout

\begin_layout Standard
There is like polar coordinates.
 - it is bad because different observers (in different locations) won't
 agree on the location of a star.
 - The rotation of the earth is another problem.
 (See p.
 15)
\end_layout

\begin_layout Subsection
The Equatorial Coordinate System
\end_layout

\begin_layout Standard
(p.
 26)
\end_layout

\begin_layout Standard
The celestial equator is the plane perpendicular to north-south line.
 People in Ecuador (that are on the equator) are always on the celestial
 equator.
\end_layout

\begin_layout Standard
\begin_inset Formula $\delta$
\end_inset

 is the angle of the star above the celestial equator.
 
\begin_inset Formula $\gamma$
\end_inset

 points to the vernal equinox - the vector between earth and the sun when
 winter ends on the 
\begin_inset Formula $21$
\end_inset

st of March.
 The ascension (right ascension, positive according to the right hand rule),
 is marked by 
\begin_inset Formula $\alpha$
\end_inset

.
 
\begin_inset Formula $\alpha$
\end_inset

 usually measured in 
\begin_inset Quotes eld
\end_inset

time
\begin_inset Quotes erd
\end_inset

, i.e.
 
\begin_inset Formula $24h$
\end_inset

 is 
\begin_inset Formula $360^{\circ}$
\end_inset

.
\end_layout

\begin_layout Standard
Meridian is the great circle passing through the celestial poles and the
 zenith and nadir (what's up and down for the observer).
 (see 
\begin_inset CommandInset href
LatexCommand href
target "https://en.wikipedia.org/wiki/Meridian_(astronomy)"

\end_inset

 )
\end_layout

\begin_layout Standard
The local sidereal time of the observer is the time (up to 24 hour, this
 actually measures the angle) the elapsed since the vernal equinox last
 traversed the meridian.
\end_layout

\begin_layout Standard
Precession of earth changes (slowly) the direction of the vernal equinox
 in time.
 Therefore we must also specify the time of the vernal equinox.
 A common time is 
\begin_inset Formula $01/01/2000$
\end_inset

 at noon in Greenwich (
\begin_inset Formula $J2000.0$
\end_inset

).
\end_layout

\begin_layout Subsection
Measuring time
\end_layout

\begin_layout Standard
We take time 
\begin_inset Formula $0$
\end_inset

 to be 
\begin_inset Formula $1/1$
\end_inset

, year 
\begin_inset Formula $-4713$
\end_inset

 (
\begin_inset Formula $4713$
\end_inset

 b.c.).
 This is denoted by 
\begin_inset Formula $JD\,0.0$
\end_inset

.
 Time from this 
\begin_inset Formula $t_{0}$
\end_inset

 is counted in days.
 
\begin_inset Formula $J2000.0$
\end_inset

 is 
\begin_inset Formula $\approx(2000+4713)\cdot365.25=2451923.25$
\end_inset

 The true value (not sure exactly why):
\begin_inset Formula 
\[
J2000.0=JD\,2452545.0
\]

\end_inset

 
\end_layout

\begin_layout Subsection
Measuring motion
\end_layout

\begin_layout Standard
We usually only see the tangential movement.
 We write the speed in the decomposition:
\begin_inset Formula 
\[
\vec{v}=v_{\theta}\hat{\theta}+v_{r}\hat{r}
\]

\end_inset


\begin_inset Formula 
\[
v_{\theta}=r\frac{d\theta}{dt}
\]

\end_inset

The proper motion - the angle in the sky that a star moves per unit time
 is
\begin_inset Formula 
\[
\mu\equiv\frac{d\theta}{dt}=\frac{v_{\theta}}{r}
\]

\end_inset


\end_layout

\begin_layout Standard
Using trigo (on spheres!) the distance (in angle 
\begin_inset Formula $\theta$
\end_inset

) that a star moves is
\begin_inset Formula 
\[
\left(\Delta\theta\right)^{2}=\left(\Delta\alpha\cos\delta\right)^{2}+\left(\Delta\delta\right)^{2}
\]

\end_inset


\end_layout

\begin_layout Section
Brightness and Distances
\end_layout

\begin_layout Standard
Parsec (=parallax-second) is a distance unit equal to 
\begin_inset Formula 
\[
1\,pc=2.0626...\times10^{5}\,AU=3.0856...\times10^{16}\,m
\]

\end_inset

(See p.
 79).
 It is define in a way that if 
\begin_inset Formula $p''$
\end_inset

 is the change of a star's location (in arcseconds) after 6 months, then:
\begin_inset Formula 
\[
d=\frac{1}{p''}\text{pc}
\]

\end_inset


\begin_inset Formula $1\,\text{pc}\approx3.26...\,\text{ly}$
\end_inset


\end_layout

\begin_layout Subsection
Brightness
\end_layout

\begin_layout Standard
Using Hipparchus's method.
 
\begin_inset Formula $m=1$
\end_inset

 is the brightest star in the sky (which is???), and 
\begin_inset Formula $m=6$
\end_inset

 to the dimmest (which is?).
 This is the apparent magnitude.
\end_layout

\begin_layout Standard
The scale is logarithmic, 
\begin_inset Formula $m+5$
\end_inset

 is 
\begin_inset Formula $100$
\end_inset

 times 
\series bold
less
\series default
 bright than 
\begin_inset Formula $m$
\end_inset

.
 The sum has magnitude of 
\begin_inset Formula $m=-26.83$
\end_inset

 (very bright indeed...).
\end_layout

\begin_layout Standard
What we measure here is the radiant flux 
\begin_inset Formula $F$
\end_inset

.
 It is the total energy (in all wavelengths) that crosses a unit area per
 unit time.
\end_layout

\begin_layout Standard
Luminosity is the intrinsic brightness - it's the energy emitted per unit
 time.
\end_layout

\begin_layout Standard
The inverse square law (like in waves) comes from conservation of energy,
 states that
\begin_inset Formula 
\[
F=\frac{L}{4\pi r^{2}}
\]

\end_inset

with 
\begin_inset Formula $L$
\end_inset

 the luminosity, 
\begin_inset Formula $r$
\end_inset

 the distance from the star.
\end_layout

\begin_layout Standard

\series bold
Absolute Magnitude
\end_layout

\begin_layout Standard
\begin_inset Formula $M$
\end_inset

, defined by the apparent magnitude a star would have if it were located
 at distance 
\begin_inset Formula $10\,\text{pc}$
\end_inset

.
\end_layout

\begin_layout Standard
Consider 2 stars, with 
\begin_inset Formula $F_{1},F_{2}$
\end_inset

, 
\begin_inset Formula $m_{1},m_{2}$
\end_inset

.
 the ratio between their brightness is 
\begin_inset Formula 
\[
100^{\left(m_{1}-m_{2}\right)/5}
\]

\end_inset

and this is exactly 
\begin_inset Formula $F_{2}/F_{1}$
\end_inset

:
\begin_inset Formula 
\[
\frac{F_{2}}{F_{1}}=100^{\left(m_{1}-m_{2}\right)/5}\iff
\]

\end_inset


\begin_inset Formula 
\[
m_{1}-m_{2}=-2.5\log_{10}\left(\frac{F_{1}}{F_{2}}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
Using the inverse square law, and taking 
\begin_inset Formula $M$
\end_inset

 to correspond to the brightness for 
\begin_inset Formula $10\,\text{pc}$
\end_inset

 we get the relation:
\begin_inset Formula 
\[
\frac{L}{4\pi\left(10\,\text{pc}\right)^{2}F_{1}}=100^{\left(m-M\right)/5}
\]

\end_inset


\begin_inset Formula 
\[
\frac{d^{2}}{\left(10\,\text{pc}\right)^{2}}=100^{\left(m-M\right)/5}
\]

\end_inset

where 
\begin_inset Formula $d$
\end_inset

 is the distance from the star, and 
\begin_inset Formula $m$
\end_inset

 the apparent magnitude, 
\begin_inset Formula $M$
\end_inset

 the absolute magnitude
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
d=10^{\left(m-M\right)/5}\cdot10\,\text{pc}
\]

\end_inset


\begin_inset Formula 
\[
d=10^{\left(m-M+5\right)/5}\,\text{pc}
\]

\end_inset

(But still, how can we know 
\begin_inset Formula $M$
\end_inset

?)
\end_layout

\begin_layout Standard
We found a relation between 
\begin_inset Formula $m-M$
\end_inset

 and we can define the distance by it.
 
\begin_inset Formula $m-M$
\end_inset

 is called the 
\series bold
distance modulus
\series default
.
 
\begin_inset Formula 
\[
m-M=5\log_{10}\left(d\right)-5=5\log_{10}\left(\frac{d}{10\,\text{pc}}\right)
\]

\end_inset


\end_layout

\begin_layout Standard
we will see another relation.
 
\begin_inset Formula $F_{10,\astrosun}$
\end_inset

 is the radiant flux received from the sun if it were 
\begin_inset Formula $10\,\text{pc}$
\end_inset

 away.
 
\begin_inset Formula $M_{\text{Sun}}$
\end_inset

 is the absolute magnitude of the sun.
 We actually saw that
\begin_inset Formula 
\[
m_{1}-m_{2}=-2.5\log_{10}\left(\frac{F_{1}}{F_{2}}\right)\implies
\]

\end_inset


\begin_inset Formula 
\[
m_{1}=M_{\text{Sun}}-2.5\log_{10}\left(\frac{F_{1}}{F_{10,\astrosun}}\right)
\]

\end_inset


\end_layout

\begin_layout Subsection
The Color Index
\end_layout

\begin_layout Standard
All the discussion above was true taking all wavelengths.
 They are actually called 
\series bold
bolometric magnitudes
\series default
, denoted 
\begin_inset Formula $m_{\text{bol}},M_{\text{bol}}$
\end_inset

.
\end_layout

\begin_layout Standard
There is a standard 
\begin_inset Formula $UBV$
\end_inset

 system which is 3 different wavelength filters.
 Therefore, measuring the magnitude of a star is actually measuring 
\begin_inset Formula $m_{U},m_{B},m_{V}$
\end_inset

.
 If the distance is known, the absolute color magnitude 
\begin_inset Formula $M_{U},M_{B},M_{V}$
\end_inset

 can be determined.
 
\begin_inset Formula $U-B$
\end_inset

 color index is
\begin_inset Formula 
\[
U-B=M_{U}-M_{B}
\]

\end_inset

and similar to 
\begin_inset Formula $B-V$
\end_inset

.
\end_layout

\begin_layout Standard
Bolometric Correction 
\begin_inset Formula $BC$
\end_inset

 is defined as:
\begin_inset Formula 
\[
BC=m_{\text{bol}}-V=M_{\text{bol}}-M_{V}
\]

\end_inset

(note that 
\begin_inset Formula $V$
\end_inset

 is the apparent magnitude in the visual spectrum, and one should expect
 the notation 
\begin_inset Formula $m_{V}$
\end_inset

, although it is not used).
\end_layout

\begin_layout Standard
I don't understand the arbitrary choice for additional additive constant!!!
 (see p.
 98 for example).
 - I think it is not arbitrary, they just wrote how to calibrate it according
 to the system they defined before.
\end_layout

\begin_layout Section
Opacity
\end_layout

\begin_layout Standard
See p.
 241
\end_layout

\begin_layout Standard
\begin_inset Formula $I_{\lambda}$
\end_inset

 is the intensity of the ray, the distance it travels is 
\begin_inset Formula $ds$
\end_inset

 and 
\begin_inset Formula $\rho$
\end_inset

 is the density of the gas through which it travels.
 
\begin_inset Formula $\kappa_{\lambda}$
\end_inset

 called the 
\series bold
opacity
\series default
 (=absorption coefficient).
 The intensity decreases by the relation:
\begin_inset Formula 
\[
dI_{\lambda}=-\kappa_{\lambda}\rho I_{\lambda}ds
\]

\end_inset

and the solution is exponential decay over the distance:
\begin_inset Formula 
\[
I_{\lambda}=I_{\lambda,0}e^{-\kappa_{\lambda}\rho s}
\]

\end_inset

 We can define the 
\series bold
optical depth
\series default

\begin_inset Formula 
\[
\tau_{\lambda}=\int_{0}^{s}\kappa_{\lambda}\rho ds
\]

\end_inset

and then
\begin_inset Formula 
\[
I_{\lambda}=I_{\lambda,0}e^{-\tau_{\lambda}}
\]

\end_inset

the optical depth is a measure for 
\begin_inset Quotes eld
\end_inset

how long is the distance a photon have inside the star
\begin_inset Quotes erd
\end_inset

, note that it is dimensionless, and it actually a measure for how much
 the intensity decreases when a photon travels through the atmosphere of
 a star.
 This also tells use how deep we can look inside a star, while 
\begin_inset Formula $\tau_{\lambda}=1$
\end_inset

 is usually taken as the depth.
\end_layout

\begin_layout Standard
See p.
 249.
 There are several effects contributing to 
\begin_inset Formula $\kappa$
\end_inset

.
 Taking them together, and averaging over 
\begin_inset Formula $\lambda$
\end_inset

, results in
\series bold
 Kramers opacity law
\series default
:
\begin_inset Formula 
\[
\overline{\kappa}=\kappa_{0}\frac{\rho}{T^{3.5}}
\]

\end_inset


\end_layout

\end_body
\end_document
