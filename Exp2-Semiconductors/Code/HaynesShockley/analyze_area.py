import glob
from typing import List

import matplotlib.axes
import matplotlib.figure
import matplotlib.lines
import matplotlib.pyplot as plt
import numpy as np

from HaynesShockley.analyze import Measurement


class RecombinationAnalyzer(object):
    def __init__(self, measurements: List[Measurement], area_segments, t0=0.0):
        self.measurements = measurements
        self.t0 = t0

        taus = np.zeros(len(measurements))
        areas = np.zeros(len(measurements))

        for i, meas in enumerate(self.measurements):
            a, b = area_segments[i]

            # taus[i] = meas.get_tau()
            # We can use the given interesting part to get the middle of the peak
            taus[i] = (a + b) / 2

            areas[i] = meas.get_area_between(a, b, should_plot=False)

        self.areas = areas
        self.taus = taus

        self.taus -= t0

    def plot_area_vs_taus(self, ax):
        # Change units:
        self.taus *= 1000000
        self.areas *= 1000000
        ax.plot(self.taus, self.areas, '.')

        ax.set_xlabel(r'$\tau$ $ [\mu s] $')
        ax.set_ylabel(r'area $ [V\cdot \mu s] $')
        # ax.set_title('area vs tau')


def analyze_area():
    plt.rcParams['font.size'] = 14

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)

    measurements, relevant_segments, t0 = get_measurements()

    # fig, ax = plt.subplots()
    # measurements[0].plot_Volts_vs_time(ax)
    # plt.show()

    analyzer = RecombinationAnalyzer(measurements, relevant_segments, t0)
    # plt.show()
    analyzer.plot_area_vs_taus(ax)


    fig.savefig('../../Figures/HS-area-vs-t.png')

    plt.show()


def get_measurements():
    paths = glob.glob(f'../../Data/2021-05-20/L=*.csv')
    paths.sort()

    empty_meas_path = f'../../Data/2021-05-20/empty3.csv'
    empty_meas = Measurement(empty_meas_path, clean_Fourier=False)

    paths = paths[-8:]

    t0 = -0.00014

    # As printed from find_relevant_area_segment
    relevant_segments1 = [(-0.00013139357994110354, -0.00012508265586653062),
                         (-0.0001294703954753568, -0.00012244291179380108),
                         (-0.00012772986438228237, -0.00011970688717534464),
                         (-0.0001259748183465773, -0.00011720289298457483),
                         (-0.00012373810371614272, -0.00011265302890408422),
                         (-0.00012159069317048596, -0.00011200151562475389),
                         (-0.00011928365527522178, -0.00010384269767022902),
                         (-0.00011807004049393439, -0.00010793161051904002)]

    relevant_segments3 = [(-0.0001316522508245899, -0.0001265645127107251),
                          (-0.00012990935385980912, -0.00012425623971600686),
                          (-0.0001278495714329135, -0.00012131067896073864),
                          (-0.00012571091764142437, -0.00011871612875876889),
                          (-0.00012406929883187817, -0.00011570309859245054),
                          (-0.00012151869393963968, -0.00011318386589942439),
                          (-0.00011949648949776926, -0.00010654201930599829),
                          (-0.00011796888349286972, -0.00010816226786613464)]

    relevant_segments = relevant_segments1

    return [Measurement(path, t0, empty_meas) for path in paths], relevant_segments, -0.000145


def get_measurements_for_plot_example():
    paths = glob.glob(f'../../Data/2021-05-20/L=*.csv')
    paths.sort()

    empty_meas_path = f'../../Data/2021-05-20/empty3.csv'
    empty_meas = Measurement(empty_meas_path, clean_Fourier=False)

    paths = paths[-4:]

    t0 = -0.00014

    return [Measurement(path, t0, empty_meas) for path in paths]


def find_relevant_area_segment():
    measurements, _, _ = get_measurements()
    relevant_segments = []
    for meas in measurements:
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        assert isinstance(fig, matplotlib.figure.Figure)
        meas.plot_Volts_vs_time(ax)
        points = fig.ginput(n=2)
        relevant_segments.append((points[0][0], points[1][0]))

    print(relevant_segments)


def main():
    # find_relevant_area_segment()
    analyze_area()


if __name__ == '__main__':
    main()
