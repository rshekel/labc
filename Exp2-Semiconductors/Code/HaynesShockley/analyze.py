import glob
import os
import re

import matplotlib.axes
import matplotlib.figure
import matplotlib.lines
import matplotlib.pyplot as plt
import numpy as np
from uncertainties import unumpy as unp

import fitter

path = r'../../Data/2021-05-13/scope_0.csv'


class Measurement(object):
    def __init__(self, path, t0=0.0, empty_meas=None, clean_Fourier=True):
        print(f'loading {path}')
        data = np.loadtxt(path, usecols=(0, 1, 2), delimiter=',', skiprows=4, max_rows=1990)
        time, Volts, trigger = data.transpose()
        self.time: np.ndarray = time
        self.Volts: np.ndarray = Volts
        self.trigger: np.ndarray = trigger
        self.t0 = t0
        self.path = path

        self._remove_DC()
        if empty_meas:
            self._remove_empty_meas(empty_meas)
        if clean_Fourier:
            self.clean_freqs(5e6, np.inf)

    def _remove_DC(self):
        DC = self.Volts.mean()
        self.Volts -= DC

    def _remove_empty_meas(self, empty_meas):
        assert isinstance(empty_meas, Measurement)
        self.Volts -= empty_meas.Volts

    def plot_Volts_vs_time(self, ax) -> matplotlib.lines.Line2D:
        lines = ax.plot(self.time, self.Volts)
        return lines[0]

    def plot_tau(self, ax: matplotlib.axes.Axes):
        line = ax.axvline(self.get_tau(), color='g', linestyle='--')
        return line

    def get_tau(self):
        index0 = np.where(self.time > self.t0)[0][0]
        index_of_max = np.argmax(self.Volts[index0:])
        index_of_min = np.argmin(self.Volts[index0:])

        t_max = self.time[index0 + index_of_max]
        t_min = self.time[index0 + index_of_min]

        # return (t_max + t_min) / 2 - self.t0
        return (t_max + t_min) / 2

    def get_width(self):
        index0 = np.where(self.time > self.t0)[0][0]
        index_of_max = np.argmax(self.Volts[index0:])
        index_of_min = np.argmin(self.Volts[index0:])

        t_max = self.time[index0 + index_of_max]
        t_min = self.time[index0 + index_of_min]

        return t_max - t_min

    def get_area(self):
        index0 = np.where(self.time > self.t0)[0][0]
        index_of_max = np.argmax(self.Volts[index0:])
        index_of_min = np.argmin(self.Volts[index0:])

        t_max = self.time[index0 + index_of_max]
        t_min = self.time[index0 + index_of_min]

        value_max = self.Volts[index0 + index_of_max]
        value_min = self.Volts[index0 + index_of_min]

        # return (t_max - t_min) * (value_max - value_min)
        return (t_max - t_min) * (value_max - value_min)

    def get_area_between(self, a, b, should_plot=False):
        index_a = np.where(self.time > a)[0][0]
        index_b = np.where(self.time > b)[0][0]

        values = self.Volts[index_a:index_b]
        value_a = self.Volts[index_a]
        value_b = self.Volts[index_b]

        indexes = np.arange(len(values))

        values -= value_a + (value_b - value_a) * indexes / (index_b - index_a)

        if should_plot:
            fig, ax = plt.subplots()
            ax.plot(self.time[index_a:index_b], values)
            ax.set_title(self.path)

        dt = (b - a) / len(values)
        area = sum(values) * dt  # This is discrete integration
        return -area

    def fit_to_Gaussian(self, a, b):
        index_a = np.where(self.time > a)[0][0]
        index_b = np.where(self.time > b)[0][0]

        values = np.copy(self.Volts[index_a:index_b])
        ts = np.copy(self.time[index_a:index_b])
        ts -= ts.mean()

        values -= values.max()
        values /= values.min()

        def Gaussian_func(x, x0, A, C, sigma):
            # print(np.exp, type(x), type(x0))
            return A * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2)) + C

        params = fitter.FitParams(Gaussian_func, ts, values)
        params.bounds = ((-ts[-1] / 4, 0.5, -1, 0), (ts[-1] / 4, 4, 1, ts[-1] * 2))
        fit = fitter.FuncFit(params)

        return fit

    def plot_Gaussian(self, ax, a, b, fit) -> matplotlib.lines.Line2D:
        Gaussian_xs = fit.params.xs
        x0, A, C, sigma = unp.nominal_values(fit.fit_results)
        Gaussian_ys = fit.params.fit_func(Gaussian_xs, x0, A, C, sigma)

        index_a = np.where(self.time > a)[0][0]
        index_b = np.where(self.time > b)[0][0]

        values = self.Volts[index_a:index_b]
        ts = self.time[index_a:index_b]

        Gaussian_xs += ts.mean()
        scale_factor = values.min() - values.max()
        Gaussian_ys *= scale_factor
        Gaussian_ys += values.max()

        lines = ax.plot(Gaussian_xs, Gaussian_ys, '--', label='fit to Gaussian')
        return lines[0]

    def plot_Fourier(self):
        fourier = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(self.Volts)))
        freqs = np.fft.fftshift(np.fft.fftfreq(len(self.Volts), self.time[1] - self.time[0]))

        fig, ax = plt.subplots()
        ax.plot(freqs, np.abs(fourier), '-')

        return freqs, fourier

    def clean_freqs(self, freq_min, freq_max):
        fourier = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(self.Volts)))
        freqs = np.fft.fftshift(np.fft.fftfreq(len(self.Volts), self.time[1] - self.time[0]))

        freqs_to_remove1 = np.logical_and(freq_min < freqs, freqs < freq_max)
        freqs_to_remove2 = np.logical_and(-freq_min > freqs, freqs > -freq_max)
        freqs_to_remove = np.logical_or(freqs_to_remove1, freqs_to_remove2)
        fourier[freqs_to_remove] = 0

        cleaned_data = np.fft.fftshift(np.fft.ifft(np.fft.ifftshift(fourier)))
        # freqs = np.fft.fftshift(np.fft.fftfreq(len(self.Volts), self.time[1] - self.time[0]))
        self.Volts = cleaned_data.real


class LDependenceAnalyzer(object):
    def __init__(self, paths, t0, empty_meas=None):
        self.Ls = [float(re.findall(r'.*L=(\d*\.?\d*)\.csv', path)[0]) for path in paths]
        self.measurements = [Measurement(path, t0, empty_meas) for path in paths]

        taus = np.zeros(len(self.Ls))
        for i, meas in enumerate(self.measurements):
            taus[i] = meas.get_tau()
        taus *= 1000  # Change to ms
        self.taus = taus

    def plot_all(self, ax, plot_peaks=False):
        for meas, L in zip(self.measurements, self.Ls):
            line = meas.plot_Volts_vs_time(ax)
            line.set_label(f'L={L}')

            if plot_peaks:
                color = line.get_color()
                vline = meas.plot_tau(ax)
                vline.set_color(color)

        ax.legend()

    def plot_L_vs_taus(self, ax: matplotlib.axes.Axes):
        ax.plot(self.taus, self.Ls, '.')
        ax.set_xlabel(r'$\tau$ [ms]')
        ax.set_ylabel('L [mm]')

    def fit_to_linear_L_vs_tau(self) -> fitter.FuncFit:
        linear = lambda x, a, b: a * x + b
        params = fitter.FitParams(linear, self.taus, self.Ls)
        return fitter.FuncFit(fit_params=params)

    def fit_to_linear_tau_vs_L(self) -> fitter.FuncFit:
        linear = lambda x, a, b: a * x + b
        params = fitter.FitParams(linear, self.Ls, self.taus)
        return fitter.FuncFit(fit_params=params)


class DiffusionAnalyzer(object):
    def __init__(self, paths, t0):
        self.Ls = [float(re.findall(r'.*L=(\d*\.?\d*)\.csv', path)[0]) for path in paths]
        self.measurements = [Measurement(path, t0) for path in paths]

        taus = np.zeros(len(self.Ls))
        width = np.zeros(len(self.Ls))

        for i, meas in enumerate(self.measurements):
            taus[i] = meas.get_tau()
            width[i] = meas.get_width()

        self.taus = taus
        self.widths = width

    def plot_tau_vs_width(self, ax: matplotlib.axes.Axes):
        ax.plot(self.widths, self.taus, '.')
        ax.set_title(r'$\tau$ vs pulse width')
        # TODO: Add units

    def plot_tau_2_vs_width(self, ax: matplotlib.axes.Axes):
        ax.plot(self.taus, self.widths ** 2, '.')
        ax.set_title(r'pulse width ^ 2 vs $\tau$')

    def fit_to_sqrt(self):
        def _sqrt(x, A, C, d):
            return A * np.sqrt(x + d) + C

        def _linear(x, a, b):
            return a * x + b

        params = fitter.FitParams(_sqrt, self.widths, self.taus)
        params.bounds = ((0.01, 0, 0), (0.2, 1, 2e-5))
        fit = fitter.FuncFit(params)
        return fit


def rename_files():
    paths = [f'../../Data/2021-05-13/scope_{i}.csv' for i in range(1, 15)]
    Ls = [2, 2.5, 3, 3.5, 4, 4.5, 5, 6, 7, 8, 9, 10, 11, 12]
    for i, path in enumerate(paths):
        os.rename(path, f'../../Data/2021-05-13/L={Ls[i]}.csv')


def plot_all():
    fig, ax = plt.subplots()

    # paths = [f'../../Data/2021-05-13/scope_{i}.csv' for i in range(1, 15)]
    # paths = glob.glob(f'../../Data/2021-05-13/Set1/L=*.csv')
    # paths = glob.glob(f'../../Data/2021-05-13/Set2/L=*.csv')
    paths = glob.glob(f'../../Data/2021-05-20/L=*.csv')
    paths.sort()

    empty_meas_path = f'../../Data/2021-05-20/empty3.csv'
    empty_meas = Measurement(empty_meas_path, clean_Fourier=False)

    paths = paths[::2]
    # paths = paths[-8:]

    # t0 = 0.000291
    t0 = -0.00014

    analyzer = LDependenceAnalyzer(paths, t0, empty_meas)
    analyzer.plot_all(ax, plot_peaks=True)
    ax.axhline(0, color='b', linestyle='--')

    fig, ax = plt.subplots()
    analyzer.plot_L_vs_taus(ax)

    plt.show()


def test_Fourier():
    path = f'../../Data/2021-05-13/Set1/L=02.5.csv'
    t0 = 0.000291
    meas = Measurement(path, t0)

    peak1 = 0.0002868
    peak2 = 0.0002797

    print(f'expected freq: {1 / (peak2 - peak1)}')

    freqs, fourier = meas.plot_Fourier()

    freq_min = 140000 - 20000
    freq_max = 140000 + 20000

    freqs_to_remove1 = np.logical_and(freq_min < freqs, freqs < freq_max)
    freqs_to_remove2 = np.logical_and(-freq_min > freqs, freqs > -freq_max)
    freqs_to_remove = np.logical_or(freqs_to_remove1, freqs_to_remove2)
    # freqs[freqs_to_remove] = 0
    fourier[freqs_to_remove] = 0

    fig, ax = plt.subplots()
    ax.plot(freqs, np.abs(fourier))

    cleaned_data = np.fft.fftshift(np.fft.ifft(np.fft.ifftshift(fourier)))
    # freqs = np.fft.fftshift(np.fft.fftfreq(len(self.Volts), self.time[1] - self.time[0]))

    fig, ax = plt.subplots()
    ax.plot(meas.time, cleaned_data)
    ax.plot(meas.time, meas.Volts)

    plt.show()


def test_empty_meas():
    empty_meas1 = Measurement('../../Data/2021-05-20/empty1.csv')
    empty_meas2 = Measurement('../../Data/2021-05-20/empty2.csv')
    empty_meas3 = Measurement('../../Data/2021-05-20/empty3.csv')

    fig, ax = plt.subplots()
    empty_meas1.plot_Volts_vs_time(ax)
    empty_meas2.plot_Volts_vs_time(ax)
    empty_meas3.plot_Volts_vs_time(ax)

    # plt.show()
    path = '../../Data/2021-05-20/L=02.0.csv'
    meas1 = Measurement(path, empty_meas=empty_meas2)
    meas1_uncleaned = Measurement(path)

    fig, ax = plt.subplots()

    meas1.plot_Volts_vs_time(ax)
    meas1_uncleaned.plot_Volts_vs_time(ax)

    meas1.plot_Fourier()
    meas1.clean_freqs(5e6, np.inf)
    meas1.plot_Volts_vs_time(ax)

    plt.show()


def analyze_drift():
    fig, ax = plt.subplots()

    # paths = [f'../../Data/2021-05-13/scope_{i}.csv' for i in range(1, 15)]
    paths = glob.glob(f'../../Data/2021-05-13/Set1/L=*.csv')
    # paths = glob.glob(f'../../Data/2021-05-13/Set2/L=*.csv')
    paths.sort()

    print(paths)

    # paths = paths[::3]

    t0 = 0.000291

    analyzer = LDependenceAnalyzer(paths, t0)
    # analyzer.plot_all(ax)
    analyzer.plot_L_vs_taus(ax)

    plt.show()


def analyze_diffusion():
    fig, ax = plt.subplots()

    paths = glob.glob(f'../../Data/2021-05-13/Set1/L=*.csv')
    # paths = glob.glob(f'../../Data/2021-05-13/Set2/L=*.csv')
    paths.sort()

    # paths = paths[::3]

    t0 = 0.000291

    analyzer = DiffusionAnalyzer(paths, t0)

    # analyzer.plot_tau_vs_width(ax)
    analyzer.plot_tau_2_vs_width(ax)
    plt.show()

    fit = analyzer.fit_to_sqrt()
    fit.print_results()
    fit.plot_fit(ax)
    fit.plot_data(ax)

    plt.show()


def calc_mobility():
    v_drift = 1  # TODO: Fix
    V_sweep = 1  # TODO: Fix
    # mobility = d * L / (t * V_sweep)

    # D / mobility should be 0.026 Volt


def plot_cleaning_for_report():
    plt.rcParams['font.size'] = 14
    path = f'../../Data/2021-05-20/L=09.0.csv'

    empty_meas_path = f'../../Data/2021-05-20/empty3.csv'
    empty_meas = Measurement(empty_meas_path, clean_Fourier=False)

    uncleaned = Measurement(path=path)
    with_empty = Measurement(path=path, empty_meas=empty_meas, clean_Fourier=False)
    and_fourier = Measurement(path=path, empty_meas=empty_meas, clean_Fourier=True)

    uncleaned.Volts *= 1e3
    with_empty.Volts *= 1e3
    and_fourier.Volts *= 1e3

    t0 = -0.00014

    uncleaned.time -= t0
    with_empty.time -= t0
    and_fourier.time -= t0

    uncleaned.time *= 1e6
    with_empty.time *= 1e6
    and_fourier.time *= 1e6

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)

    # line4 = empty_meas.plot_Volts_vs_time(ax)
    # line4.set_label("empty meas")

    line1 = uncleaned.plot_Volts_vs_time(ax)
    line2 = with_empty.plot_Volts_vs_time(ax)
    line3 = and_fourier.plot_Volts_vs_time(ax)

    line1.set_label(r'raw signal')
    line2.set_label(r'after subtraction')
    line3.set_label(r'after Fourier')

    line3.set_linewidth(2.5)

    ax.legend()

    # ax.set_xlim((-0.000138 - t0) * 1e6, (-0.000120 - t0) * 1e6)
    ax.set_ylim(-5, 5)
    ax.set_xlim(8, 25)

    ax.set_xlabel('t [$ \mu $s]')
    ax.set_ylabel('Volts [mV]')

    fig.savefig(r'../../Figures/HS-cleaning-process.png')

    plt.show()


def plot_drift_for_report():
    plt.rcParams['font.size'] = 14

    fig, ax = plt.subplots()

    # paths = glob.glob(f'../../Data/2021-05-13/Set2/L=*.csv')
    paths = glob.glob(f'../../Data/2021-05-13/Set1/L=*.csv')
    paths.sort()

    paths.remove(paths[1])

    t0 = 0.000291

    analyzer = LDependenceAnalyzer(paths, t0)
    # analyzer.plot_all(ax)
    # analyzer.plot_L_vs_taus(ax)

    fit = analyzer.fit_to_linear_tau_vs_L()
    line = fit.plot_data(ax, label='data')
    fit.plot_fit(ax, label='linear fit')

    ax.legend()
    ax.set_ylabel(r'$\tau$ [ms]')
    ax.set_xlabel('d [mm]')

    a, b = fit.fit_results
    v_d = 1 / a
    print(f'drift velocity: {v_d}')
    print(f'{v_d:L}')

    fig.savefig(r'../../Figures/HS-drift.png')

    plt.show()


def main():
    # test_Fourier()
    # test_empty_meas()
    # plot_all()
    # analyze_area()
    # analyze_diffusion()
    # analyze_drift()
    # plot_cleaning_for_report()
    plot_drift_for_report()


if __name__ == '__main__':
    main()
    # rename_files()
