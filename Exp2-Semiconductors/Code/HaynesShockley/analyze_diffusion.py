import glob
import re

import matplotlib.axes
import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np
import uncertainties
import matplotlib.markers
from uncertainties import unumpy as unp

import fitter
from HaynesShockley.analyze import Measurement


def get_measurements():
    paths = glob.glob(f'../../Data/2021-05-20/L=*.csv')
    paths.sort()

    empty_meas_path = f'../../Data/2021-05-20/empty3.csv'
    empty_meas = Measurement(empty_meas_path, clean_Fourier=False)

    paths = paths[-8:]

    t0 = -0.00014

    relevant_segments2 = [(-0.00013138605753446907, -0.00012526498561390135),
                          (-0.0001303345578805862, -0.00012227047575612222),
                          (-0.00012752862730610755, -0.00011946519133721628),
                          (-0.00012580483231544495, -0.00011730953557722031),
                          (-0.00012367929009745197, -0.0001126036922337932),
                          (-0.00012163731733352907, -0.00011015735396631302),
                          (-0.00011946660480253158, -0.00010376323399636053),
                          (-0.00011796646040947206, -0.00010433667687477604)]

    relevant_segments3 = [(-0.0001316522508245899, -0.0001265645127107251),
                          (-0.00012990935385980912, -0.00012425623971600686),
                          (-0.0001278495714329135, -0.00012131067896073864),
                          (-0.00012571091764142437, -0.00011871612875876889),
                          (-0.00012406929883187817, -0.00011570309859245054),
                          (-0.00012151869393963968, -0.00011318386589942439),
                          (-0.00011949648949776926, -0.00010654201930599829),
                          (-0.00011796888349286972, -0.00010816226786613464)]

    relevant_segments = relevant_segments3

    index_to_remove = 6  # This is bad...
    paths.remove(paths[index_to_remove])
    relevant_segments.remove(relevant_segments[index_to_remove])

    return [Measurement(path, t0, empty_meas) for path in paths], relevant_segments, -0.000145


def analyze_diffusion():
    plt.rcParams['font.size'] = 14

    measurements, relevant_segments, t0 = get_measurements()
    widths = unp.uarray(np.zeros(len(measurements)), np.zeros(len(measurements)))
    taus = np.zeros(len(measurements))

    for i in range(len(widths)):
        a, b = relevant_segments[i]
        # This is a naive, and incorrect approach
        # widths[i] = (b - a)
        # widths[i] = (b - a) / 2.5
        widths[i] = get_FWHM_by_Gaussian(measurements[i], a, b)
        # print(widths[i] / (b-a))
        taus[i] = get_tau_by_Gaussian(measurements[i], a, b)
        # taus[i] = (a + b) / 2

    taus -= t0

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)

    # Change to micro second
    widths *= 1e6
    taus *= 1e6

    # ax.plot(widths ** 2, taus, '.')

    linear = lambda x, a, b: a * x + b
    params = fitter.FitParams(linear, taus, widths ** 2)
    fit = fitter.FuncFit(params)
    fit.print_results()
    a, b = fit.fit_results
    # Note: b is not 0; which is a problem...

    fit.plot_data(ax, 'data')
    fit.plot_fit(ax, 'linear fit')

    ax.legend()
    ax.set_ylabel(r'$ {w}^{2} $ [$\left(\mu s\right)^{2}$]')
    ax.set_xlabel(r'$ \tau $ [$\mu s$]')

    fig.savefig(r'../../Figures/HS-width2-vs-tau.png')

    # Now we will try to extract D diffusion from here
    v_d = uncertainties.ufloat(365, 8)  # In m/s
    # v_d /= 1e6  # In m / (micro second)
    v_d *= 1e2  # In cm/s

    # a=\frac{16\ln2\cdot D}{v_{d}^{2}}
    # D=\frac{av_{d}^{2}}{16\ln2}
    D = a / 1e6 * v_d ** 2 / (16 * np.log(2))
    print(f'Diffusion: {D} cm^2/s')
    # Should be around ~30 cm^2/s

    plt.show()


def test_fit_to_Gaussian():
    measurements, relevant_segments, t0 = get_measurements()

    indexes = range(len(measurements))
    for i in indexes:
        meas = measurements[i]
        relevant_segment = relevant_segments[i]

        a, b = relevant_segment

        fig, ax = plt.subplots()

        fit = meas.fit_to_Gaussian(a, b)
        # fit.plot_data(ax)
        # fit.plot_fit(ax)
        meas.plot_Volts_vs_time(ax)
        meas.plot_Gaussian(ax, a, b, fit)

        fit.print_results()
        print(f'Our width is {b - a}')
        # See https://en.wikipedia.org/wiki/Full_width_at_half_maximum for the relation between sigma and FWHM
        print(f'FWHM: {fit.fit_results[3] * 2.355}')

    plt.show()


def get_FWHM_by_Gaussian(meas, a, b):
    fit = meas.fit_to_Gaussian(a, b)
    sigma = fit.fit_results[3]
    return 2.355 * sigma


def get_tau_by_Gaussian(meas, a, b):
    fit = meas.fit_to_Gaussian(a, b)
    x0 = fit.fit_results[0]
    print(x0, (b + a) / 2)
    return (b + a) / 2 + x0.n


def plot_all():
    measurements, relevant_segments, t0 = get_measurements()
    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    for i, meas in enumerate(measurements):
        line = meas.plot_Volts_vs_time(ax)
        a, b = relevant_segments[i]
        vline1 = ax.axvline(a, linestyle='--', color=line.get_color())
        vline2 = ax.axvline(b, linestyle='--', color=line.get_color())

    plt.show()


def plot_example_for_report():
    plt.rcParams['font.size'] = 14

    measurements, relevant_segments, t0 = get_measurements()
    measurements = [measurements[0], measurements[2], measurements[4], measurements[6]]
    relevant_segment = relevant_segments[2]

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)

    for i, meas in enumerate(measurements):
        meas.time -= t0
        meas.time *= 1e6
        meas.Volts *= 1e3

        line = meas.plot_Volts_vs_time(ax)
        d = float(re.findall(r'.*L=(\d*\.?\d*)\.csv', meas.path)[0])
        line.set_label(f'd={d} [mm]')
        if i == 1:
            a, b = relevant_segment
            a -= t0
            b -= t0
            a *= 1e6
            b *= 1e6

            ax.axvline(a, linestyle='--', color=line.get_color())
            ax.axvline(b, linestyle='--', color=line.get_color())
            center = (a + b) / 2
            ax.plot(center, -0.00365 * 1e3, marker=matplotlib.markers.CARETUP, color='m', markersize=12)

    meas = measurements[1]
    a, b = relevant_segment
    a -= t0
    b -= t0
    a *= 1e6
    b *= 1e6

    fit = meas.fit_to_Gaussian(a, b)
    line = meas.plot_Gaussian(ax, a, b, fit)
    # line.set_color('g')
    line.set_linewidth(2.5)

    ax.set_xlim((-0.0001397 - t0) * 1e6, (-0.0001 - t0) * 1e6)
    ax.set_ylim(-4.1, 3)

    ax.set_xlabel('t [$ \mu $s]')
    ax.set_ylabel('Volts [mV]')

    ax.legend()

    fig.savefig(r'../../Figures/HS-examples.png')

    plt.show()


def main():
    # analyze_diffusion()
    # test_fit_to_Gaussian()
    # plot_all()
    plot_example_for_report()


if __name__ == '__main__':
    main()
