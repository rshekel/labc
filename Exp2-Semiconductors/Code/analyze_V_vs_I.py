import re

import matplotlib.pyplot as plt
import numpy as np
import uncertainties.unumpy

import fitter


def analyze(path, R):
    const_B = float(re.findall(r'.*B=(\d*\.?\d*).txt', path)[0])
    const_B *= 10  # machine measurement factor
    const_B *= 1e-4  # Gauss to Tesla

    data = np.loadtxt(path, usecols=(0, 1), delimiter=',', skiprows=1)
    I, V = data.transpose()

    # Error in voltage measurement of 1/3 of last digit
    errors = np.ones(V.shape) * 0.03
    V_errors = uncertainties.unumpy.uarray(V, errors)

    # There was also some error in the x-axis, imposing I, of 0.003
    errors = np.ones(I.shape) * 0.003
    I_errors = uncertainties.unumpy.uarray(I, errors)

    def _linear(xs, a, b):
        return a * xs + b

    params = fitter.FitParams(_linear, I_errors, V_errors)
    linear_fit = fitter.FuncFit(params)

    a, b = linear_fit.fit_results

    hall_const = (a - R) / const_B
    hall_const_without_fix = a / const_B
    # hall_const = a / const_B
    print(f"Hall: {hall_const}")
    print(f"Hall - without fix: {hall_const_without_fix}")

    fig, ax = plt.subplots()

    ax.set_xlabel("I (mA)")
    ax.set_ylabel("V (mV)")
    ax.set_title(f"B={const_B} Tesla, Hall {hall_const}")

    linear_fit.plot_data(ax, label='data')
    linear_fit.plot_fit(ax, label='linear fit')

    ax.legend()

    fig_no_fix, ax_no_fix = fig, ax

    fig, ax = plt.subplots()

    ax.set_title('Residues for V vs I')
    ax.set_ylabel('residues [mV]')
    ax.set_xlabel('I [mA]')

    linear_fit.plot_residues(ax)

    linear_fit.print_results()
    linear_fit.print_results_latex()

    fig, ax = plt.subplots()

    V_errors -= R * I

    params = fitter.FitParams(_linear, I_errors, V_errors)
    linear_fit = fitter.FuncFit(params)
    linear_fit.plot_data(ax)
    linear_fit.plot_fit(ax)
    print('With IR')
    linear_fit.print_results()
    ax.set_title('V Hall, taking IR into account')

    return fig_no_fix, ax_no_fix

    # plt.show()


def meeting2():
    # path = '../Data/2021-05-06/V_vs_I_B=100.txt'
    # path = '../Data/2021-05-06/V_vs_I_B=200.txt'
    path = '../Data/2021-05-06/V_vs_I_B=300.txt'
    # R = 11.7
    # R = 10
    R = -10
    # R = -10.48
    # R = uncertainties.ufloat(-10.48, 0.04)

    analyze(path, R)

    # Hall: 22.9+/-1.0
    # Hall: 24.33+/-0.14
    # Hall: 23.41+/-0.29

    # Without I fix:
    # Hall: -77.1+/-1.0
    # Hall: -25.67+/-0.14
    # Hall: -9.92+/-0.29


def meeting1():
    I0 = 3
    V0 = -4.6
    R = V0 / I0
    print(f'R: {R}')

    path = '../Data/2021-05-03/V_vs_I_B=150.txt'
    # path = '../Data/2021-05-03/V_vs_I_B=350.txt'
    # path = '../Data/2021-05-03/V_vs_I_B=250.txt'

    analyze(path, R)

    # No R factor
    # Hall: -0.000522+/-0.00006
    # Hall: -0.0003253+/-0.000022
    # Hall: -0.000978+/-0.00005

    # With R effect:
    # Hall: (4.5+/-0.5)e-05
    # Hall: 0.0001128+/-0.0000022
    # Hall: (9.1+/-0.6)e-05


def plot_for_report():
    plt.rcParams['font.size'] = 14

    # path = '../Data/2021-05-06/V_vs_I_B=100.txt'
    path = '../Data/2021-05-06/V_vs_I_B=200.txt'
    # path = '../Data/2021-05-06/V_vs_I_B=300.txt'
    # R = -10
    R = uncertainties.ufloat(-10.0, 0.1)

    fig, ax = analyze(path, R)
    ax.set_title('')
    ax.set_xlabel('I [mA]')
    ax.set_ylabel('V [mV]')

    fig.savefig(r'../Graphs/Hall-V-vs-I.png')

    print('with B=0.2 Tesla')

    plt.show()


def main():
    # meeting2()
    plot_for_report()


if __name__ == '__main__':
    main()
