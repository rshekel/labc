import matplotlib.axes
import matplotlib.pyplot as plt
import numpy as np
import uncertainties.unumpy

import fitter


def plot_T_vs_T_emf():
    path = '../Data/2021-05-03/V_vs_T_B=250_I=7mA.csv'

    data = np.loadtxt(path, usecols=(0, 1, 2), delimiter=',', skiprows=1)
    T_emf, V, T = data.transpose()

    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    ax.plot(T_emf, T, '.')
    ax.set_xlabel('T-emf [mV]')
    ax.set_ylabel('T [C]')
    ax.set_title('T vs T-emf')

    plt.show()


def plot_V_vs_T(path):
    data = np.loadtxt(path, usecols=(0, 1, 2), delimiter=',', skiprows=1)
    T_emf, V, T = data.transpose()

    errors = np.ones(V.shape) * 0.03
    V_errors = uncertainties.unumpy.uarray(V, errors)

    def _linear(xs, a, b):
        return a * xs + b

    T += 273.15

    params = fitter.FitParams(_linear, T, V_errors)
    # params = fitter.FitParams(_linear, T, 1/V)
    # params.bounds = ((0, 0.02, -1), (20, 1e3, 0.01))
    linear_fit = fitter.FuncFit(params)

    fig, ax = plt.subplots()

    linear_fit.plot_data(ax)
    # linear_fit.plot_fit(ax)
    linear_fit.print_results()
    linear_fit.print_results_latex()

    a, b = linear_fit.fit_results

    # ax.set_xlabel("I (mA)")
    # ax.set_ylabel("V (mV)")
    # ax.set_title(f"B={const_B} Gauss, Hall {hall_const}")

    plt.show()


def main():
    # path = '../Data/2021-05-03/V_vs_T_B=250_I=7mA.csv'
    path = '../Data/2021-05-06/V_vs_T_B=0_I=7mA.csv'
    # plot_T_vs_T_emf()
    plot_V_vs_T(path)


if __name__ == '__main__':
    main()
