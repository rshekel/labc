import numpy as np
import uncertainties


def compare_different_Hall_constants():
    c = [22.9,
         24.33,
         23.41,
         23.8,
         23.9,
         23.1]
    hall = np.array(c)
    print(f'{hall.mean()}+-{hall.std()}')
    print(f'min, max: {hall.min()}, {hall.max()}')

    # Without fix we have

    # Hall: -77.1+/-1.0
    # Hall: -25.67+/-0.14
    # Hall: -9.92+/-0.29
    # and the true value, 23.5...


def calc_n():
    q = 1.60217662e-19  # Charge of electron

    t = uncertainties.ufloat(1, 0.3)  # in mm
    t /= 1000  # Change to m

    Hall_const = uncertainties.ufloat(23.5, 0.6)

    R_h = Hall_const * t
    n = 1 / (R_h * q)  # in 1/(m^3)

    n /= 1e6  # Change 1/m^3 to 1/cm^3
    print(f'{n:L}')


def main():
    # compare_different_Hall_constants()
    calc_n()


if __name__ == '__main__':
    main()
