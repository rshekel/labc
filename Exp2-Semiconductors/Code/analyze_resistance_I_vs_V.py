import matplotlib.pyplot as plt
import numpy as np
import uncertainties.unumpy

import fitter


def analyze(path):
    data = np.loadtxt(path, usecols=(0, 1), delimiter=',', skiprows=1)
    I, V = data.transpose()

    # Error in voltage measurement of 1/3 of last digit
    errors = np.ones(V.shape) * 0.03
    V_errors = uncertainties.unumpy.uarray(V, errors)

    # There was also some error in the x-axis, imposing I, of 0.003
    errors = np.ones(I.shape) * 0.003
    I_errors = uncertainties.unumpy.uarray(I, errors)

    def _linear(xs, a, b):
        return a * xs + b

    params = fitter.FitParams(_linear, I_errors, V_errors)
    linear_fit = fitter.FuncFit(params)

    a, b = linear_fit.fit_results

    fig, ax = plt.subplots()

    ax.set_xlabel("I (mA)")
    ax.set_ylabel("V (mV)")
    ax.set_title(f"Resistance, no magnetic field")

    linear_fit.plot_data(ax, label='data')
    linear_fit.plot_fit(ax, label='linear fit')

    ax.legend()

    fig, ax = plt.subplots()

    ax.set_title('Residues for V vs I')
    ax.set_ylabel('residues [mV]')
    ax.set_xlabel('I [mA]')

    linear_fit.plot_residues(ax)

    linear_fit.print_results()
    linear_fit.print_results_latex()

    plt.show()


def main():
    # path = '../Data/2021-05-06/Resistance_V_vs_I2.csv'
    path = '../Data/2021-05-06/Resistance_V_vs_I_Temp_probe.csv'

    analyze(path)


if __name__ == '__main__':
    main()
