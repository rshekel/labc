import re

import matplotlib.pyplot as plt
import numpy as np
import uncertainties.unumpy

import fitter


def analyze(path, R=None):
    const_I = float(re.findall(r'.*I=(\d*\.?\d*)mA.csv', path)[0])

    data = np.loadtxt(path, usecols=(0, 1), delimiter=',', skiprows=1)
    B, V = data.transpose()

    B *= 10  # machine measurement factor
    B *= 1e-4  # Gauss to Tesla

    errors = np.ones(V.shape) * 0.1
    V_errors = uncertainties.unumpy.uarray(V, errors)

    def _linear(xs, a, b):
        return a * xs + b

    # if R:
    #     V_errors -= const_I * R

    params = fitter.FitParams(_linear, B, V_errors)
    linear_fit = fitter.FuncFit(params)

    fig, ax = plt.subplots()

    linear_fit.plot_data(ax, 'data')
    linear_fit.plot_fit(ax, 'linear fit')
    linear_fit.print_results()
    linear_fit.print_results_latex()

    a, b = linear_fit.fit_results
    hall_const = a / const_I
    print(f"Hall: {hall_const}")

    print(f'slope normalized: {a / const_I}')

    ax.set_xlabel("B (Gauss)")
    ax.set_ylabel("V (mV)")
    ax.set_title(f"I={const_I}mA. Hall {hall_const}")
    ax.legend()

    fig_res, ax_res = plt.subplots()

    ax_res.set_title('Residues for V vs B')
    ax_res.set_ylabel('residues [mV]')
    ax_res.set_xlabel('I [mA]')

    linear_fit.plot_residues(ax_res)

    return fig, ax, fig_res, ax_res


def meeting1():
    path = '../Data/2021-05-03/V_vs_B_I=3mA.csv'
    # path = '../Data/2021-05-03/V_vs_B_I=10mA.csv'
    # path = '../Data/2021-05-03/V_vs_B_I=15mA.csv'

    analyze(path)

    # Constant I:
    # Hall: 0.0001661+/-0.000017
    # Hall: 0.0001655+/-0.000006
    # Hall: 0.0001659+/-0.0000009

    # Constant B:
    # Hall: -0.000522+/-0.00006
    # Hall: -0.0003253+/-0.000022
    # Hall: -0.000978+/-0.00005


def meeting2():
    # path = '../Data/2021-05-06/V_vs_B_I=4mA.csv'
    # path = '../Data/2021-05-06/V_vs_B_I=8mA.csv'
    path = '../Data/2021-05-06/V_vs_B_I=12mA.csv'

    analyze(path)
    plt.show()

    # Hall: 23.8+/-0.6
    # Hall: 23.9+/-0.7
    # Hall: 23.1+/-0.6


def plot_for_report():
    plt.rcParams['font.size'] = 14

    path = '../Data/2021-05-06/V_vs_B_I=4mA.csv'

    fig, ax, fig_res, ax_res = analyze(path)
    ax.set_title('')
    ax.set_xlabel('B [T]')
    ax.set_ylabel('V [mV]')

    fig.savefig(r'../Figures/Hall-V-vs-B.png')

    ax_res.set_title('')
    ax_res.set_xlabel('B [T]')
    ax_res.set_ylabel('Residuals - V [mV]')
    fig_res.savefig(r'../Figures/Hall-V-vs-B-residues.png')

    print('with I=4mA')

    v = uncertainties.ufloat(-40, 0.6)

    print(f"R by y intersection is {v / 4}")

    plt.show()


def main():
    # meeting2()
    plot_for_report()


if __name__ == '__main__':
    main()
