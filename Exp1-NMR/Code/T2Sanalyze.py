import matplotlib.axes
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit

import fitter


class T2SAnalyzer(object):
    # _exp_fit_bounds = ((9, 300, -10), (20, 500, 10))  # A, L, x0
    _exp_fit_bounds = ((9, -3, 0), (20, 3, 5000))  # A, C, T2S
    _gaus_fit_bounds = ((9, 300, -10), (20, 500, 10))  # A, L, x0
    _Lorentzian_fit_bounds = ((9, 0, 1 / 1000), (20, 1, 1))  # A, x0, T2S

    def __init__(self, data_path):
        data = np.loadtxt(data_path, usecols=(3, 4), delimiter=',')
        self.ts, self.volts = data.transpose()

        # Begin fit only from max magnetization
        self.start_fit_index = self.volts.argmax()

    def plot_volts_vs_ts(self, fit_to='exp'):
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)

        ts_ms = self.ts * 1000

        ax.plot(ts_ms, self.volts, label='data')

        ax.set_xlabel("t [ms]")
        ax.set_ylabel("Volts [V]")

        if fit_to == 'exp':
            popt = self.fit_to_exp()
            ts = self.ts[self.start_fit_index:]
            fit_vs = self._exp(ts, *popt)
            ax.plot(ts, fit_vs, label=r'fit to $ M_{0}\exp\left(-\frac{t}{T_{2}^{*}}\right)+C $')
            ax.set_title("Fit to Exp")
        elif fit_to == 'gaus':
            popt = self.fit_to_gaus()
            ts = self.ts[self.start_fit_index:]
            fit_vs = self._gaus(ts, *popt)
            ax.plot(ts, fit_vs)
            ax.set_title("Fit to Gaussian")
        elif fit_to == 'Lorentzian':
            popt = self.fit_to_Lorentzian()
            ts = self.ts[self.start_fit_index:]
            fit_vs = self._Lorentzian(ts, *popt)
            ax.plot(ts, fit_vs)
            ax.set_title("Fit to Lorentzian")

        ax.legend()

        return fig, ax

    def _exp(self, x, A, C, T2S):
        return A * np.exp(-x / T2S) + C

    def _gaus(self, x, A, L):
        return A * np.exp(-L * (x) ** 2)

    def _Lorentzian(self, x, A, x0, T2S):
        return A / (1 + (x - x0) / T2S)

    def fit_to_exp(self):
        ts = self.ts[self.start_fit_index:]
        ts *= 1000
        vs = self.volts[self.start_fit_index:]
        popt, pcov = curve_fit(self._exp, ts, vs, bounds=self._exp_fit_bounds)

        params = fitter.FitParams(self._exp, ts, vs)
        params.bounds = self._exp_fit_bounds
        fit = fitter.FuncFit(params)
        fit.print_results()
        fit.print_results_latex()

        return popt

    def fit_to_gaus(self):
        ts = self.ts[self.start_fit_index:]
        vs = self.volts[self.start_fit_index:]
        popt, pcov = curve_fit(self._gaus, ts, vs)  # , bounds=self._gaus_fit_bounds)
        print(popt)
        return popt

    def fit_to_Lorentzian(self):
        ts = self.ts[self.start_fit_index:]
        vs = self.volts[self.start_fit_index:]
        popt, pcov = curve_fit(self._Lorentzian, ts, vs, bounds=self._Lorentzian_fit_bounds)
        print(popt)
        return popt


def main():
    plot_for_paper()


def plot_for_paper():
    plt.rcParams['font.size'] = 14

    path = "../Data/2021-04-05/MineralOil/MineralOil_T2S.csv"
    analyzer = T2SAnalyzer(path)
    fig, ax = analyzer.plot_volts_vs_ts(fit_to='exp')
    ax.set_title('$ T_{2}^{*} $ Measurement')
    # analyzer.plot_volts_vs_ts(fit_to='gaus')
    # analyzer.plot_volts_vs_ts(fit_to='Lorentzian')

    fig.savefig(r'../Graphs/W1_T2S_exp_fit.png')

    plt.show()


if __name__ == '__main__':
    main()
