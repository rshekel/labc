import numpy as np 
import matplotlib.pyplot as plt 

# Gauss distribution of frequencies 
B = np.random.randn(500)

# plt.hist(B, bins=50)
# plt.show()

# Evaluate at different times 
t = np.linspace(0, 10, 100)

# each row is vectors at certains time 
Z = np.exp(1j*np.outer(t, B))

# Sum of all vectors for each t 
Y = Z.sum(1)

fig, ax = plt.subplots()
# Plot abs value of sum vector 
power = np.abs(Y) ** 2
ax.plot(power)
fig.show()
