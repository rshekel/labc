import glob
import os
import re

import matplotlib.axes
import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import find_peaks
from uncertainties import ufloat

import fitter


def _exp(x, A, L):
    return A * np.exp(-L * x)


class PulseTrainT2Analyzer(object):
    def __init__(self, data_path):
        self.path = data_path
        data = np.loadtxt(data_path, usecols=(3, 4), delimiter=',')
        self.ts, self.volts = data.transpose()

        # filename is something like 'PulseTrain_tau=5ms_n=7.csv'
        self.tau = float(re.findall(r'.*tau=(\d*\.?\d*)ms.*', data_path)[0]) * 1e-3
        self.N = int(re.findall(r'.*n=(\d+).csv.*', data_path)[0])

    def plot_volts_vs_ts(self):
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        ax.plot(self.ts, self.volts)
        ax.set_title("T2 measurement")
        ax.set_xlabel("t [s]")
        ax.set_ylabel("V [Volts]")

    def plot_peaks(self):
        fig, ax = plt.subplots()
        ax.plot(self.ts, self.volts)
        peak_ts, peak_vs = self.get_peaks()
        ax.plot(peak_ts, peak_vs, '*')
        ax.set_title(self.path)
        return ax

    def plot_fit(self):
        fig, ax = plt.subplots()
        ax.plot(self.ts, self.volts)
        peak_ts, peak_vs = self.get_peaks()
        ax.plot(peak_ts, peak_vs, '*')
        popt = self.fit_to_exp(peak_ts, peak_vs)
        start_fit_index = np.where(self.ts == peak_ts[0])[0][0]
        fit_vs = _exp(self.ts[start_fit_index:], *popt)
        ax.plot(self.ts[start_fit_index:], fit_vs)

    def get_peaks(self, ignore_start_peak=False):
        """ Return ts, vs of peaks"""
        peak_indexes = find_peaks(self.volts, width=2, prominence=0.05)[0]
        peak_times = self.ts[peak_indexes]

        # print(self.tau)
        # print(np.abs(np.mod(peak_times, (2 * self.tau - self.tau * 0.001))))
        peak_times = list(filter(lambda x: np.abs(x % (2 * self.tau - self.tau * 0.001)) < self.tau / 15, peak_times))

        if ignore_start_peak:
            peak_times = list(filter(lambda x: x > self.tau / 6, peak_times))

        peak_vs = [self.volts[np.where(self.ts == peak_time)][0] for peak_time in peak_times]

        return peak_times[:self.N + 1], peak_vs[:self.N + 1]  # Till N+1 because there is also the "A" peak

    def get_peaks2(self, ignore_start_peak=False):
        peak_indexes = find_peaks(self.volts, width=5)[0]
        peak_times = self.ts[peak_indexes]
        peak_times = list(filter(lambda x: np.abs(x % (2 * self.tau - self.tau * 0.001)) < self.tau / 4, peak_times))
        peak_vs = [self.volts[np.where(self.ts == peak_time)][0] for peak_time in peak_times]

        if ignore_start_peak:
            peak_times = peak_times[1:]
            peak_vs = peak_vs[1:]

        # return peak_times[:self.N + 1], peak_vs[:self.N + 1]  # Till N+1 because there is also the "A" peak
        return peak_times[:self.N], peak_vs[:self.N]  # Till N+1 because there is also the "A" peak

    def fit_to_exp(self, ts, vs):
        popt, pcov = curve_fit(_exp, ts, vs)
        return popt


def plot_fit_single_file():
    base_path = "../Data/2021-04-05/MineralOil/T2try1"
    paths = glob.glob(os.path.join(base_path, "PulseTrain*"))
    path = paths[-1]
    analyzer = PulseTrainT2Analyzer(path)
    # analyzer.plot_volts_vs_ts()
    # analyzer.plot_peaks()
    analyzer.plot_fit()


def plot_single_file():
    # path = r"../Data/2021-04-08/Water/T2_single_pulse/SinglePulse_tau=130ms_n=1.csv"
    path = r"../Data/2021-04-08/Water/T2_pulse_train/PulseTrain_tau=25ms_n=17.csv"
    analyzer = PulseTrainT2Analyzer(path)
    # analyzer.plot_volts_vs_ts()
    analyzer.plot_peaks()


def T2_from_all_trains():
    plt.rcParams['font.size'] = 14

    base_path = "../Data/2021-04-05/MineralOil/T2try1"
    base_path = "../Data/2021-04-05/MineralOil/T2try2"
    # base_path = "../Data/2021-04-08/MineralOil/T2_no_train"

    fig, ax = plt.subplots()
    plot_from_all_trains(base_path, ax)

    ax.set_title(r'$ T_2 $ Measurement using Pulse Train')

    fig.savefig(r'../Graphs/W2-T2-pulse-train.png')
    fig.show()


def plot_from_all_trains(base_path, ax, peak_method=1, should_fit=True):
    peak_ts = np.array([])
    peak_vs = np.array([])

    print(base_path)

    paths = glob.glob(os.path.join(base_path, "*.csv"))
    for path in paths:
        print(path)
        an = PulseTrainT2Analyzer(path)
        # an.plot_peaks()

        if peak_method == 1:
            ts, vs = an.get_peaks(ignore_start_peak=True)
        elif peak_method == 2:
            ts, vs = an.get_peaks2(ignore_start_peak=True)
        else:
            raise RuntimeError()

        print(ts, vs)

        ts = np.array(ts)
        ts *= 1000  # change to ms

        # normalize t
        # ts -= ts[0]
        # Remove first pi/2 peak
        # ts = ts[1:]
        # vs = vs[1:]
        # print(ts)
        peak_ts = np.append(peak_ts, ts)
        peak_vs = np.append(peak_vs, vs)

    ax.plot(peak_ts, peak_vs, '*', label='data')

    ax.set_xlabel("t [ms]")
    ax.set_ylabel("Volts [V]")
    ax.set_title("T2 measurement - from all pulse trains")

    # popt, pcov = curve_fit(_exp, peak_ts, peak_vs)
    # X = np.linspace(peak_ts[0], peak_ts[-1], 1000)
    # Y = _exp(X, *popt)
    # ax.plot(X, Y)

    if should_fit:
        fit_params = fitter.FitParams(_exp, peak_ts, peak_vs)
        fit = fitter.FuncFit(fit_params)
        fit.print_results()
        tau = 1 / fit.fit_results[1]
        print(f"tau is {tau:L}")
        fit.plot_fit(ax, label=r'fit to $ M_{0}\exp\left(-\frac{t}{T_{2}}\right) $')

    ax.legend()
    return peak_ts, peak_vs


def plot_graph_W2():
    plt.rcParams['font.size'] = 14

    base_path = "../Data/2021-04-08/MineralOil/T2_no_train"
    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)
    plot_from_all_trains(base_path, ax)
    ax.set_title(r"T2 Measurement - Using Single Echo")

    # Calc sigma error
    a = ufloat(0.03232, 0.00034)
    b = ufloat(0.0343, 0.0007)
    print(f"sigma err: {np.abs((b - a).std_score(0))}")

    fig.savefig("../Graphs/W2-T2-1pulse.png")


def plot_T2_water():
    base_path = r"../Data/2021-04-08/Water/T2_single_pulse"
    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)
    peak_ts, peak_vs = plot_from_all_trains(base_path, ax, peak_method=1, should_fit=False)
    ax.set_title(r"T2 measurement - Water - Using 1 pulse with different $ \tau $")

    # exp_3 = lambda x, A, tau, tau2, x0, C: A * np.exp(-(x - x0)**3 / tau**3 - (x - x0) / tau2) + C
    # fit_params.bounds = ((0, 0.1, 20, 10, -20), (100000, 600, 1000, 40, 20))
    exp_3 = lambda x, A, tau, x0, C: A * np.exp(-(x - x0) ** 3 / tau ** 3) + C

    # ax.set_xlim(min(peak_ts)-1, max(peak_ts) + 20)

    fit_params = fitter.FitParams(exp_3, peak_ts, peak_vs)
    fit_params.bounds = ((0, 0.1, -200, -20), (100, 6000, 100, 20))
    fit = fitter.FuncFit(fit_params)
    fit.print_results()
    tau = fit.fit_results[1]
    print(f"tau is {tau:L}")
    fit.plot_fit(ax, label=r'fit to $ A\cdot\exp\left(-\frac{\left(t-t_{0}\right)^{3}}{\tau^{3}}\right)+C $')

    ax.legend()

    fig.savefig("../Graphs/T2-water-single-echo.png")


def plot_T2_water_pulse_train():
    plt.rcParams['font.size'] = 14

    base_path = r"../Data/2021-04-08/Water/T2_pulse_train"
    fig, ax = plt.subplots()
    assert isinstance(ax, matplotlib.axes.Axes)
    assert isinstance(fig, matplotlib.figure.Figure)
    peak_ts, peak_vs = plot_from_all_trains(base_path, ax, peak_method=1, should_fit=False)

    # Ignore the last peak, since it seems unrelated
    good = peak_ts < 15000
    peak_ts = peak_ts[good]
    peak_vs = peak_vs[good]

    ax.set_xlim(0, 1100)

    def _exp_tau(x, A, tau):
        return A * np.exp(-x / tau)

    fit_params = fitter.FitParams(_exp_tau, peak_ts, peak_vs)
    fit_params.bounds = ((0, 20), (110, 800))
    fit = fitter.FuncFit(fit_params)
    fit.print_results()
    tau = fit.fit_results[1]
    print(f"tau is {tau:L}")
    fit.plot_fit(ax, label=r'fit to $ M_{0}\exp\left(-\frac{t}{T_{2}}\right) $')

    ax.legend()
    ax.set_title(r"T2 measurement - Water - Pulse Train")
    # fig.savefig("../Graphs/T2-water-pulse_train.png")




def main():
    # plot_fit_single_file()
    # T2_from_all_trains()
    # plot_graph_W2()
    # plot_T2_water()
    plot_T2_water_pulse_train()
    # plot_single_file()


if __name__ == '__main__':
    main()
    plt.show()
