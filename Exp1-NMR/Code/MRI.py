import matplotlib.axes
import matplotlib.figure
import matplotlib.lines
import matplotlib.pyplot as plt
import numpy as np
import uncertainties


class MRIAnalyzer(object):
    def __init__(self, path, normalize=False):
        self.path = path
        data = np.loadtxt(path, usecols=(3, 4, 10), delimiter=',')
        self.ts, self.Ch1, self.volts = data.transpose()

        print(self.Ch1[:10])
        print(len(self.Ch1))

        if normalize:
            self.volts /= self.Ch1

    def plot_volts_vs_ts(self):
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        ax.plot(self.ts, self.volts)
        ax.set_title("MRI measurement")
        ax.set_xlabel("t [s]")
        ax.set_ylabel("V [Volts]")

    def plot_Ch1_volts_vs_ts(self):
        fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)
        ax.plot(self.ts, self.Ch1)
        ax.set_title("Channel 1")
        ax.set_xlabel("t [s]")
        ax.set_ylabel("V [Volts]")

    def plot_Fourier(self, ax: matplotlib.axes.Axes = None):
        interesting = self.ts > 0
        ts = self.ts[interesting]
        volts = self.volts[interesting]

        # fourier = np.fft.fft(volts)
        fourier = np.fft.fftshift(np.fft.fft(np.fft.ifftshift(volts)))
        freqs = np.fft.fftshift(np.fft.fftfreq(len(volts), ts[1] - ts[0]))

        freqs /= 1000  # Change to KHz

        if ax is None:
            fig, ax = plt.subplots()

        # ax.plot(freqs, np.abs(fourier), '.')
        ax.plot(freqs, np.abs(fourier))
        ax.set_title("MRI measurement - Fourier")
        ax.set_xlabel("frequency [KHz]")
        ax.set_ylabel("abs(FFT)")


def main():
    # plot_MRI_for_paper()
    # plot_2_measurements()
    # plt.show()
    calc_n_sigmas()


def plot_Fourier():
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/grady-try1.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/try2-no-grad.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/try2-with-grad.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/try3-grad-long-time-10kAM.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/try4-no-AM.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/try5.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/try6.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-12/try7.csv")
    # analyzer = MRIAnalyzer(r"../Data/2021-04-19/Calibration-MRI/d=3.5cm.csv")
    # analyzer.plot_volts_vs_ts()

    # analyzer = MRIAnalyzer(r"../Data/2021-04-19/MRI_MEAS/try1.csv", normalize=False)
    analyzer = MRIAnalyzer(r"../Data/2021-04-19/MRI_MEAS/try5,d=4.5cm,AM58.csv", normalize=False)
    # analyzer = MRIAnalyzer(r"../Data/2021-04-19/MRI_MEAS/try6,d=4.4cm,AM58.csv", normalize=False)
    analyzer.plot_Fourier()
    analyzer.plot_volts_vs_ts()
    analyzer.plot_Ch1_volts_vs_ts()
    plt.show()


def plot_2_measurements():
    fig, ax = plt.subplots()
    analyzer1 = MRIAnalyzer(r"../Data/2021-04-19/Calibration-MRI/try3/d=3.5cm.csv")
    analyzer2 = MRIAnalyzer(r"../Data/2021-04-19/Calibration-MRI/try3/d=4.5cm.csv")
    analyzer1.plot_Fourier(ax)
    analyzer2.plot_Fourier(ax)

    plt.show()


def plot_MRI_for_paper():
    plt.rcParams['font.size'] = 14

    analyzer = MRIAnalyzer(r"../Data/2021-04-19/MRI_MEAS/try5,d=4.5cm,AM58.csv", normalize=False)

    fig, ax = plt.subplots()
    assert isinstance(fig, matplotlib.figure.Figure)
    assert isinstance(ax, matplotlib.axes.Axes)

    analyzer.plot_Fourier(ax)
    # ax.set_title("Vial Imaging - Fourier Transform of the Magnetization")
    ax.set_title("")
    ax.set_xlim(20, 43)
    ax.set_ylim(0, 5)

    l1_x1, l1_x2 = 30.14, 31.480
    l2_x1, l2_x2 = 34.05, 35.82
    # l3_x1, l3_x2 = (l1_x1+l1_x2)/2, (l2_x1+l2_x2)/2
    l3_x1, l3_x2 = l1_x2, l2_x1
    l1, = ax.plot((l1_x1, l1_x2), (2.368, 2.368), '--')
    l2, = ax.plot((l2_x1, l2_x2), (2.368, 2.368), '--')
    l3, = ax.plot((l3_x1, l3_x2), (2.368, 2.368), '--')

    assert isinstance(l1, matplotlib.lines.Line2D)
    assert isinstance(l2, matplotlib.lines.Line2D)
    assert isinstance(l3, matplotlib.lines.Line2D)

    l1.set_color('#ee0000')
    l3.set_color('#00aa00')

    c1 = l1.get_color()
    c2 = l3.get_color()
    l2.set_color(c1)

    ax.annotate('A', (29, 2.45), color=c1, fontsize=20)
    ax.annotate('B', (36.1, 2.45), color=c1, fontsize=20)
    ax.annotate('C', (32.3, 2.00), color=c2, fontsize=20)

    # Calcs:
    KHz_to_cm = uncertainties.ufloat(3.3, 0.2)
    w1 = (l1_x2 - l1_x1) / KHz_to_cm * 10
    w2 = (l2_x2 - l2_x1) / KHz_to_cm * 10
    w3 = (l3_x2 - l3_x1) / KHz_to_cm * 10

    print(w1, w2, w3)
    print(f'{w1:L}')
    print(f'{w2:L}')
    print(f'{w3:L}')

    fig.savefig('../Graphs/MRI_Fourier.png')


def calc_n_sigmas():
    a = uncertainties.ufloat(4, 0.3)
    b = uncertainties.ufloat(4, 0.3)
    print(f'1: {calc_n_sigma(a, b)}')

    a = uncertainties.ufloat(5.5, 0.3)
    b = uncertainties.ufloat(5.4, 0.3)
    print(f'2: {calc_n_sigma(a, b)}')

    a = uncertainties.ufloat(7.8, 0.5)
    b = uncertainties.ufloat(7, 0.3)
    print(f'3: {calc_n_sigma(a, b)}')


def calc_n_sigma(a: uncertainties.ufloat, b: uncertainties.ufloat):
    return abs((a.n - b.n) / np.sqrt(a.s ** 2 + b.s ** 2))


if __name__ == '__main__':
    main()
