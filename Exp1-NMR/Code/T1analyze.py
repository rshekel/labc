import glob
import os
import re

import matplotlib.axes
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import find_peaks
from uncertainties import ufloat

import fitter


def _exp(x, A, tau, x0):
    return A * (1 - np.exp(-(x - x0) / tau))


class T1SingleMeasurement(object):
    def __init__(self, data_path):
        self.path = data_path
        data = np.loadtxt(data_path, usecols=(3, 4), delimiter=',')
        self.ts, self.volts = data.transpose()

        # filename is something like 'PulseTrain_tau=5ms_n=7.csv'
        self.tau = float(re.findall(r'.*Tau=(\d*\.?\d*)ms.*', data_path)[0]) * 1e-3

    def plot_volts_vs_ts(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()
            assert isinstance(ax, matplotlib.axes.Axes)
        ax.plot(self.ts, self.volts)
        ax.set_title(r"$ T_{1} $ measurement - $ \tau= $" + f"{self.tau}" + " [s]")
        ax.set_xlabel("t [s]")
        ax.set_ylabel("V [Volts]")

    def plot_peak(self):
        fig, ax = plt.subplots()
        self.plot_volts_vs_ts(ax)
        peak_ts, peak_vs = self.get_peak()
        ax.plot(peak_ts, peak_vs, '*')

    def get_peak(self):
        """ Return ts, vs of peaks"""
        peak_indexes = find_peaks(self.volts, width=5, prominence=0.1)[0]
        peak_times = self.ts[peak_indexes]

        # tau_error = max(self.tau / 4, 0.0005)
        # peak_times = list(filter(lambda x: np.abs(x - (self.tau - self.tau * 0.001)) < tau_error, peak_times))
        # peak_vs = [self.volts[np.where(self.ts == peak_time)][0] for peak_time in peak_times]

        # Get the peak closest to the expected time, of tau
        tau_peak_index = np.argmin(np.abs(peak_times - self.tau))

        return peak_times[tau_peak_index], self.volts[peak_indexes[tau_peak_index]]  # There should be only one peak


class T1Analyzer(object):
    def __init__(self, paths):
        self.measurements = [T1SingleMeasurement(path) for path in paths]
        self.taus = [measurement.tau for measurement in self.measurements]
        self.taus_ms = [tau * 1000 for tau in self.taus]
        self.peaks = [measurement.get_peak()[1] for measurement in self.measurements]

    def plot_peaks(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()
        assert isinstance(ax, matplotlib.axes.Axes)

        ax.plot(self.taus_ms, self.peaks, '.', label='data')
        ax.set_xlabel(r"$ \tau $ [ms]")
        ax.set_ylabel("Volts [V]")
        ax.set_title("T1 measurement")

    def fit_to_exp(self, ax=None):
        if ax is None:
            fig, ax = plt.subplots()
        self.plot_peaks(ax)

        params = fitter.FitParams(_exp, self.taus_ms, self.peaks)
        # params.bounds = ((0, 0.02, -1), (20, 1e3, 0.01))
        exp_fit = fitter.FuncFit(params)
        exp_fit.plot_fit(ax, label=r'fit to $ M_{0}\left(1-\exp\left(-\frac{\tau}{T_{1}}\right)\right) $')
        exp_fit.print_results()
        exp_fit.print_results_latex()
        return ax


def plot_single_file():
    # base_path = "../Data/2021-04-05/MineralOil/T1-half-pi-half-pi"
    base_path = "../Data/2021-04-08/MineralOil/T1-pi-half-pi"
    # paths = glob.glob(os.path.join(base_path, "T1*"))
    # path = paths[10]

    path = r"../Data/2021-04-08/MineralOil/T1-pi-half-pi/T1,Tau=0.5ms.csv"
    analyzer = T1SingleMeasurement(path)
    # analyzer.plot_volts_vs_ts()
    print(analyzer.get_peak())
    analyzer.plot_peak()


def analyze_T1():
    plt.rcParams['font.size'] = 14

    base_path = "../Data/2021-04-05/MineralOil/T1-half-pi-half-pi"
    paths = glob.glob(os.path.join(base_path, "T1*"))
    analyzer = T1Analyzer(paths)

    fig, ax = plt.subplots()
    # ax.xaxis.label.set_fontsize(14)
    # ax.yaxis.label.set_fontsize(14)

    analyzer.fit_to_exp(ax)

    ax.set_title(r"$ T_1 $ Measurement using $ \frac{\pi}{2} $ Pulse")
    ax.legend()

    fig.savefig('../Graphs/W1_T1.png')


def analyze_T1_pi_half_pi():
    base_path = "../Data/2021-04-08/MineralOil/T1-pi-half-pi"
    paths = glob.glob(os.path.join(base_path, "T1*csv"))
    analyzer = T1Analyzer(paths)
    analyzer.plot_peaks()


def plot_T1_using_pi_pulse():
    plt.rcParams['font.size'] = 14

    base_path = "../Data/2021-04-08/MineralOil/T1-pi-half-pi"
    paths = glob.glob(os.path.join(base_path, "T1*csv"))
    analyzer = T1Analyzer(paths)
    fig, ax = plt.subplots()

    # plt.rcParams['axes.labelsize'] = 30
    ax.xaxis.label.set_fontsize(14)
    ax.yaxis.label.set_fontsize(14)

    analyzer.plot_peaks(ax)

    ax.set_title('$ T_1 $ Measurement using $ \pi $ Pulse')

    fig.savefig("../Graphs/W2-T1-pi-pulse.png")

    # Calc T1:
    t_zero = ufloat(0.029, 0.001)
    print(f'{t_zero / np.log(2):L}')


def calcs():
    a = ufloat(0.029, 0.001)
    b = np.log(2)
    print(a / b)


def main():
    # calcs()
    analyze_T1()
    # plot_single_file()
    # analyze_T1_pi_half_pi()
    # plot_T1_using_pi_pulse()


if __name__ == '__main__':
    main()
    plt.show()
