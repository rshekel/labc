\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{multicol,caption}
\usepackage{graphicx}
\usepackage{hyperref}
\graphicspath{./figures/}
\usepackage{geometry}
\usepackage{natbib}
\usepackage[colorinlistoftodos]{todonotes}

 \geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }

\newenvironment{Figure}
  {\par\medskip\noindent\minipage{\linewidth}}
  {\endminipage\par\medskip}


\title{Using Nuclear Magnetic Resonance for 1-D Imaging }
\author{Ronen Shekel 309987493, Alon Sardas 205947906}
\date\today


\begin{document}

\maketitle

\begin{abstract}
    Nuclear magnetic resonance is a well known phenomenon occurring in materials with both angular momentum and a magnetic moment. When a varying magnetic field is applied in a resonant frequency, we are able to control the magnetization orientation of these materials. Using the resonance phenomena, we perform 1-d imaging, where we differentiate spatially between lipstick and teflon. 
\end{abstract}

\begin{multicols}{2}

\section{Introduction}
We follow Shlichter in describing the theoretical background of NMR \cite{slichter2013principles}. Consider a magnetic system that possesses angular momentum, such as the nucleus of the hydrogen atom. In this system, the spin of the nucleus contributes to the angular momentum $ \vec{J} $ resulting in a magnetic dipole moment $ \vec{\mu} $. $ \vec{J} $ and $ \vec{\mu} $ are always parallel and are related by:
\begin{equation}\label{eq. gyromagnetic relation}\vec{\mu}=\gamma\vec{J}\end{equation}
where $ \gamma $ is called the gyro-magnetic ratio and is determined by the properties of the nucleus\footnote{In Quantum Mechanics, the angular momentum is quantized but we are going to treat the system classically, since the conclusions are the same\cite{slichter2013principles}.}. 

\subsection{Magnetic Moment in a Constant External Magnetic Field}
We consider a magnetic moment in an external magnetic field $\vec{B_0}=B_0\hat{z}$. The field applies torque $ \vec{\tau} $ on the magnetic moment, forcing it to rotate. The torque is given by $\vec{\tau}=\vec{\mu}\times\vec{B}_0$ and obeys $\vec{\tau}=\frac{d\vec{J}}{dt}$. Using Eq. \ref{eq. gyromagnetic relation} we conclude the rotation of the magnetic moment satisfy the equation:  $$ \vec{\mu}\times\vec{B}_0=\frac{1}{\gamma}\frac{d\vec{\mu}}{dt} $$

Thus, the magnetic moment will perform precessional motion with precessional frequency of $\omega_{0}=\gamma B_{0}$, called the Larmor frequency. The precessional motion is depicted in Figure \ref{fig:precession_ilust}.
 
 \begin{Figure}
 \centering
 \includegraphics[width=0.4\linewidth]{figures/precession_ilustration.PNG}
 \captionof{figure}{Illustration of the precessional motion of a magnetic moment in a constant external magnetic field.}
 \label{fig:precession_ilust}
\end{Figure}

This precessional motion assumes an isolated moment, with no energy exchanges. However, in the case of a macroscopic material the total energy of the system is related to the total magnetization $\vec{M}$ by $U=-\vec{M}\cdot\vec{B}$, so minimal energy is achieved when the moment is parallel to the magnetic field. Consider a material beginning with no magnetization. Upon turning on the magnetic field, the material will gradually become magnetized, exchanging energy and angular momentum with the material lattice, until reaching thermal equilibrium\footnote{The total magnetization in thermal equilibrium is easily computed using Boltzmann factors, and the fact that the two energy levels are separated by $\Delta U=\hbar \gamma B_0$. We get $M\propto\tanh\left(\frac{\mu B_{0}}{k_{B}T}\right)$ where $k_B$ is the Boltzmann constant, and T is the temperature.}. The relaxation time for this process is hence called the spin-lattice relaxation time and is denoted by $T_{1}$. 

\subsection{Resonance}
We add to the constant magnetic field a rotating one, so  $\vec{B}=B_{0}\hat{z}+B_{1}\cos\left(\omega t\right)\hat{x}+B_{1}\sin\left(\omega t\right)\hat{y}$, with $\omega$ the frequency of the rotating field. For simplicity, we switch to a rotating (non inertial) frame of reference in the XY plane with the same frequency $\omega$, which introduces a fictitious magnetic field of the form $\vec{B}_f=-\frac{\omega}{\gamma}\hat{z}$. We denote the coordinates in the rotating frame as $\hat{x}^{*},\hat{y}^{*}$, while $\hat{z}$ remains the same. 

The effective field in the rotating frame of reference is: $$\underbrace{B_{1}\hat{x}^{*}}_{\mbox{rotating field}}+\left(\underbrace{B_{0}}_{\mbox{constant field}}-\underbrace{\frac{\omega}{\gamma}}_{\mbox{fictitious field}}\right)\hat{z}^{*}$$ We note that the rotating magnetic field appears stationary in the $\hat{x}^{*}$ direction in the rotating frame of reference, and that the constant and fictitious fields are parallel. Hence, for a well chosen $\omega=\gamma B_{0}$ we get a so called resonance. In resonance, regardless of the magnitude of $B_1$, the effective magnetic field in the rotating frame will have only a $\hat{x}^{*}$ component. by the same analysis as before, the moment would precess around the effective magnetic field in frequency of $\Omega=\gamma B_{1}$, so the total motion is of precession around $\hat{x}^{*}$ together with a global rotation around $\hat{z}^{*}$.

We see that applying a varying field, we can rotate the total magnetization around the $\hat{x}^{*}$ axis, with frequency $\Omega=\gamma B_{1}$. If we apply this field for the correct duration, we can move the magnetization from the $\hat{z}$ direction to the $\hat{y}^{*}$ direction. We call this type of rotation a $\frac{\pi}{2}$ pulse. Applying it for enough time to flip to $-\hat{z}$ will be labeled a $\pi$ pulse. In order to get a $\pi/2$ pulse, the duration of the pulse should be $t_{\pi/2}=\frac{\pi}{2\Omega}$ and for a $\pi$ pulse, the duration should be $t_{\pi}=\frac{\pi}{\Omega}$

Naively, after applying a $\frac{\pi}{2}$ pulse to a macroscopic material, we expect all the magnetic moments to rotate together in the XY plane forever. However, two different effects cause the XY magnetization to decay. The first effect is due to in-homogeneity of the magnetic field, which causes different magnetic moments to precess in different frequencies, causing the moments to split out of phase, hence causing the overall magnetization to decay. The characteristic time for this decay is denoted $T_2^*$ . Another effect taking place is the interaction between the small magnetic fields induced by each magnetic moment and its neighbors, causing random perturbations to the magnetic field. The characteristic time for this effect is denoted $T_2$ and is called the spin-spin relaxation time. Typically $T_2$ is significantly longer than $T_2^*$.

\subsection{Echos} \label{sec:echo}
It is hard to measure the $T_2$ time, because the $T_2^*$ effect ruins the XY magnetization faster. To overcome this problem we use the echo method, as originally introduced by Hahn \cite{hahn1953free}. The dephasing due to the in-homogeneity of the magnetic field is deterministically dependant on the location of spins within the field, so it can be reversed in the following way: First, we induce a $\frac{\pi}{2}$ pulse, so the total magnetization is in the XY plane. All moments precess around the $\hat{z}$ direction for a time $t=\tau$ with slightly different frequencies, causing dephasing. At time $\tau$ we apply a $\pi$ pulse, which will cause all moments to precess in the same frequency as before (since it is location dependant) but at the opposite direction. Hence, at time $t=2\tau$ they will return to be in phase, and we will see another peak in the magnetization measurement in the XY plane. This return to phase is called an echo. The peak at the echo ($t=2\tau$) will be lower than the first one, due to the spin-spin relaxation, and this decrease is exactly how one can measure the $T_2$ spin-spin effect. This dephasing and rephasing process is depicted in Figure \ref{fig:rephasing}.

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/rephasing.PNG}
 \captionof{figure}{Using the echo method to measure $T_2$. (a) The magnetization begins in the $\hat{z}$ direction. (b) After applying a $\frac{\pi}{2}$ pulse it is in the XY plane. (c) In-homogeneity in the magnetic field causes different precession frequencies, so during a time $\tau$ dephasing occurs. (d) Applying a $\pi$ pulse reverses the direction of precession. (e) At time $2\tau$ the  different moments return to be in phase.}
 \label{fig:rephasing}
\end{Figure}

In Appendix \ref{appendix:decay} we perform measurements of $T_1,T_2,T_2^*$, and provide further explanations. 

\section{Methods}
In this work we use NMR to perform 1-D imaging, based on the reaction of the material to the magnetic field. Instead of using a homogeneous constant magnetic field, we induce a gradient in the $\hat{y}$ direction. As mentioned above, the Larmor frequency is proportional to the magnetic field: $\omega_0 \propto B_0$, so in each location in space the magnetization should precess in a different frequency. Hence, by measuring the magnetization, and identifying the different frequencies appearing in the signal, we identify the locations where there exists the "active" material that has a Larmor frequency in the range we measure. The straight forward way to extract the amplitude of each frequency is using the Fourier transform on the magnetization signal. Furthermore, by measuring the exact amplitude of each frequency - we can theoretically calculate the density of the reactant materials. This is especially convenient for medical imaging \cite{mansfield1977medical}. 

The experimental setup is depicted in Figure \ref{fig:setup}. We insert a vial parallel to $\hat{y}$ into a box with a magnetic field of our choosing, and are able to measure the magnetic field in the XY plane.

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/experimental system.JPG}
 \captionof{figure}{The experimental setup. (A) The magnet where we put the vial into. (B) The control over the temperature and magnetic field gradients in the magnet. (C) The electronics inputting the varying signal pulses, and outputting the magnetization measurement to an oscilloscope (not appearing in the figure).}
 \label{fig:setup}
\end{Figure}

We apply a gradient in the field in the $\hat{y}$ direction, and begin by measuring the Larmor frequency ($\gamma B_0$) for different locations. In each measurement we place a thin layer of lipstick in different heights, and apply a $\pi/2$ pulse to set the magnetization to the XY plane. The magnetization in the XY plane rotates around $\hat{z}$ in the Larmor frequency, which is then measured.

We find that the frequency changes approximately linearly with the location, with a slope of $0.33\pm0.02\,[KHz/mm]$. 

Next, we fill a vial with a thin layer of $\approx5.5\pm0.3\,mm$ of lipstick, whose Larmor frequencies we will measure. We then add a layer of $\approx7.0\pm0.3\,mm$ of teflon whose Larmor frequency is out of our range of measurement. Finally we add another layer of $\approx4.0\pm0.3\,mm$ of lipstick. The vial is shown in Figure \ref{fig:vial}.

\begin{Figure}
 \centering
 \includegraphics[width=0.6\linewidth]{figures/vial3.png}
 \captionof{figure}{The vial we performed the imaging on. The first layer of lipstick is in between the green lines, and the second layer in between the orange lines. In between is a layer of teflon, whose Larmor frequency is outside the range we measure.}
 \label{fig:vial}
\end{Figure}

Ideally we would expect to measure in Fourier-space two Rect functions of width $\approx0.33\times4=1.32\,KHz$ and $\approx1.82\,KHz$, with a gap between them of $\approx2.31\,KHz$. 

\section{Results}
We insert the vial into the magnetic field, and measure the magnetization. The Fourier transform of the measured magnetization is depicted in Figure \ref{fig:MRI_Fourier}.

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/MRI_Fourier.png}
 \captionof{figure}{The Fourier transform of the measured magnetization in the XY plane. We observe two distinct peaks, corresponding to the two layers of reactive material in the vial. The width of the frequency peaks correspond to the two lipstick layers.} 
 \label{fig:MRI_Fourier}
\end{Figure}  

Line A in Figure \ref{fig:MRI_Fourier} corresponds to the upper lipstick layer and the width of the peak is translated to $4.0 \pm 0.3 [mm]$. Line B corresponds to the lower lipstick layer and the width of the peak is translated to $ 5.4 \pm 0.3 [mm] $. Line C is the distance between the 2 peaks and its length corresponds to $7.8 \pm 0.5 [mm]$. All measurements agree reasonably with measurements on the actual vial performed with a ruler, with $N_{\sigma}$ of 0, 0.2 and 1.4 respectively.

\section{Conclusions}
In this work we used the phenomena of nuclear magnetic resonance, and demonstrated a proof of concept method for 1-D binary imaging. Given more accurate measurements and stronger field gradients this method can simply be extrapolated to a "gray-scale" measurement of the density of active materials. Advanced methods based on these principles allow also 2-D and 3-D imaging. NMR imaging revolutionized medical imaging, and is used every day in modern hospitals. 

\appendix

\section{Measurements of decay rates\label{appendix:decay}}
We performed measurements of the different decay rates $T_1,T_2,T_2^*$ for mineral oil using several methods, and measured $T_2$ in the case of distilled water. We explain the different methods, and show the results below. In all measurements the signal we measure is given in Volts, which is proportional to the magnetization. 

\subsection{Mineral oil}
We begin with measuring the spin-lattice relaxation time $T_1$. In this measurement we want to measure the characteristic time for the building of magnetization parallel to the magnetic field $\vec{B_0}$. We assume the rate of change in the total magnetization is proportional to its difference from its equilibrium value: 

\begin{equation}\label{eq:T1}
\frac{dM_{z}}{dt}=\frac{M_{0}-M_{z}}{T_{1}}
\end{equation} with $M_0$ the equilibrium magnetization. The solution to this equation is of course $M_{z}\left(t\right)=M_{0}\left(1-e^{-\frac{t}{T_{1}}}\right)$. 
Our first method for measuring $T_1$ will be applying a $\pi/2$ pulse, so $M_z=0$, wait a time $\tau$ so $M_z$ will begin rebuilding according to Eq. \ref{eq:T1}, and then in order to measure $M_z$ we apply another $\pi/2$ pulse and perform the measurement in the XY plane. We do this for different values of $\tau$, and we reconstruct the exponential form, as depicted in Figure \ref{fig:T1_oil_v1}.

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/W1_T1.png}
 \captionof{figure}{The building of equilibrium magnetization in the direction parallel to $\vec{B_0}$, using the $\pi/2$ pulse method.} \label{fig:T1_oil_v1}
\end{Figure}

The parameter $T_1$ is found to be: $29 \pm 1\,ms$. 

Next, we measure the same quantity in another method. We begin with a full $\pi$ pulse, so the magnetization is in the minus $\hat{z}$ direction, wait a time $\tau$, and apply another $\pi/2$ pulse to measure the $M_z$ magnetization constructed during this duration. Since we measure the absolute value of the magnetization, we expect at $\tau=0$ for the magnetization to be at its maximum, and the same for $\tau\rightarrow\infty$. It can be shown \cite{carr1954effects} that the $\tau$ for which the magnetization is exactly zero should fulfill $\tau=T_1 \cdot \ln{2}$. The measurements are depicted in Figure \ref{fig:T1_oil_v2}

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/W2-T1-pi-pulse.png}
 \captionof{figure}{The movement towards equilibrium magnetization in the direction parallel to $\vec{B_0}$, using the $\pi$ pulse method.}
 \label{fig:T1_oil_v2}
\end{Figure}

It can be seen that the $\tau$ for which the magnetization is zero is $\tau=29\pm1\,ms$, so $T_1=41\pm1\,ms$. This is not in agreement with the previous measurement, with a $\ln2$ factor, but it is in the same order of magnitude. This can be explained by the fact that the measurements were performed on different days and on different samples of mineral oil.

We continue and measure $T_2^*$. It can be shown that the functional shape of decay is the Fourier transform of the distribution of the in-homogeneity in the magnetic field. However, it is commonly considered an exponential decay. To measure this we simply apply a $\pi/2$ pulse, and measure continuously the magnetization in the XY plane. The results are depicted in Figure \ref{fig:T2S_oil}

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/W1_T2S_exp_fit.png}
 \captionof{figure}{The decay in the magnetization in the XY plane, due to the in-homogeneity in the magnetic field.} 
 \label{fig:T2S_oil}
\end{Figure} 

We can see the exponential decay is not a perfect fit. In any case the characteristic time is given by $T_2^*=2.33 \pm 0.01\,ms$.

Lastly, we measure the $T_2$ characteristic decay time. We assume the equation governing the $M_x,M_y$ magnetization is given by:
\begin{equation}\label{eq:T2}
\frac{dM_{x/y}}{dt}=-\frac{M_{x/y}}{T_{1}}
\end{equation} so the solution will be $M_{x/y}=M_{0}e^{-\frac{t}{T_{2}}}$. We measure this quantity in two methods. In the first method we use the echo effect discussed in \ref{sec:echo}, and in each measurement use a different $\tau$. The overlap of results with different $\tau$s are depicted in Figure \ref{fig:T2_oil}.

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/W2-T2-1pulse.png}
 \captionof{figure}{The decay in the magnetization in the XY plane, due to the spin-spin relaxation. We compensate for the dephasing due to in-homogeneity in the magnetic field using the echo method. } 
 \label{fig:T2_oil}
\end{Figure}

We find the spin-spin relaxation time $T_2$ to be: $T_2=32.3\pm0.3\,ms$.
Next, we use another method, using a "pulse train". This is a generalization of the echo method. In the echo method we wait a time $\tau$ before flipping the magnetization and achieving an echo at $t=2\tau$. In the pulse train we continue and apply pulses at $t=3\tau,t=5\tau$ etc. each time letting the magnetic moments to dephase and rephase, thus achieving many echoes, and performing many measurements in each run. We can perform a few pulse-trains for different values of $\tau$ to get many points in a faster manner. A graph acquired from a few different pulse trains is depicted in Figure \ref{fig:T2_oil_pulse_train}.

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/W2-T2-pulse-train.png}
 \captionof{figure}{The decay in the magnetization in the XY plane, due to the spin-spin relaxation. We compensate for the dephasing due to in-homogeneity in the magnetic field using the pulse-train method.} 
 \label{fig:T2_oil_pulse_train}
\end{Figure}

We find the spin-spin relaxation time to be $T_2=35\pm 1\,ms$, which is reasonably close to the value obtained in the single-echo method. 

We see that with agreement to the theory, $T_2^*$ is much shorter than both $T_2$ and $T_1$, and also that $T_2\leq2T_1$ \cite{levitt2013spin}.

\subsection{Distilled water}
We perform the $T_2$ measurements also on distilled water. When performing the measurements with a single echo, we do not get an exponential decay, but rather a shape of $M_{x/y}=M_{0}e^{-bt^3}$, as depicted in Figure \ref{fig:T2_water_one_echo}. 

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/T2-water-single-echo.png}
 \captionof{figure}{The decay in the magnetization in the XY plane, due to the spin-spin relaxation, and also due to diffusion, causing the $M_{0}e^{-bt^3}$ shape.} 
 \label{fig:T2_water_one_echo}
\end{Figure}

This can be explained by the fact that the water goes through a diffusion process, as explained in \cite{carr1954effects}. To overcome the diffusion effect we employ the pulse train method - and then we can see again the exponential decay, as depicted in Figure \ref{fig:T2_water_pulse_train}.
The numerical value found using the pulse train method is $T_{2} \approx300 \pm 10\,ms$.

\begin{Figure}
 \centering
 \includegraphics[width=\linewidth]{figures/T2-water-pulse_train.png}
 \captionof{figure}{The decay of the magnetization in the XY plane, due to the spin-spin relaxation, and using the pulse train we avoid the diffusion effect.} 
 \label{fig:T2_water_pulse_train}
\end{Figure}


\bibliographystyle{unsrt}
\bibliography{references}

\end{multicols}
\end{document}
